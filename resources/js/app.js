
window.Vue = require('vue');
//Import Vue Filter
require('./filter'); 

//Import progressbar
require('./progressbar'); 

//Setup custom events 
require('./customEvents'); 
Vue.config.productionTip = false
window.axios = require('axios');
window.toastr = require('toastr');
window.moment = require('moment');
import swal from 'sweetalert2';
window.Swal = swal;

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

window.Toast = Toast

//Import v-from
import { HasError, AlertError } from 'vform'
// window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import Form from './form'
window.Form= Form


//Componentes Categorias
Vue.component('categorias', require('./components/categorias/Categorias.vue').default);
Vue.component('categorias-eliminadas', require('./components/categorias/Categorias-eliminadas.vue').default);
Vue.component('panel-categorias', require('./components/principal/Panel-categorias.vue').default);

//Componentes Paieses
Vue.component('paises-eliminados', require('./components/paises/Paises-eliminados.vue').default);
Vue.component('paises-component', require('./components/paises/Paises-component.vue').default);

//Componentes Provincias
Vue.component('provincias-component', require('./components/provincias/Provincias-component.vue').default);
Vue.component('provincias-eliminadas', require('./components/provincias/Provincias-eliminadas.vue').default);

//Componentes Localidades
Vue.component('localidades-component', require('./components/localidades/Localidades-component.vue').default);
Vue.component('localidades-eliminadas', require('./components/localidades/Localidades-eliminadas.vue').default);

//Componentes de Perfiles
Vue.component('perfiles-component', require('./components/perfiles/Perfiles-component.vue').default);
Vue.component('perfiles-eliminados', require('./components/perfiles/Perfiles-eliminados.vue').default);
Vue.component('panel-perfiles', require('./components/principal/Panel-perfiles.vue').default);


//Componentes de usuarios
Vue.component('usuarios-component', require('./components/usuarios/usuarios-component.vue').default);
Vue.component('usuarios-eliminados', require('./components/usuarios/usuarios-eliminados.vue').default);
Vue.component('panel-usuarios', require('./components/principal/Panel-usuarios.vue').default);

//vComponentes de entidades
Vue.component('entidades-component', require('./components/entidades/Entidades-component.vue').default);
Vue.component('entidades-create', require('./components/entidades/Entidades-create.vue').default);
Vue.component('entidades-edit', require('./components/entidades/Entidades-edit.vue').default);
Vue.component('entidades-eliminadas', require('./components/entidades/Entidades-eliminadas.vue').default);
Vue.component('panel-entidades', require('./components/principal/Panel-entidades.vue').default);



//Componentes Articulos
Vue.component('articulos-component', require('./components/articulos/Articulo-component.vue').default);
//Vue.component('entidades-create', require('./components/entidades/Entidades-create.vue').default);
//Vue.component('entidades-edit', require('./components/entidades/Entidades-edit.vue').default);
//Vue.component('entidades-eliminadas', require('./components/entidades/Entidades-eliminadas.vue').default);
Vue.component('panel-articulos', require('./components/principal/Panel-articulos.vue').default);


Vue.component('app', require('./components/principal/Principal.vue').default);

import router from './routes'

const app = new Vue({
    
    el: '#app',
    router
});
