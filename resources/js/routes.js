import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
export default new Router ({
    routes: [
        {
            path: '/paises',
            name: 'paises',
            component: require('./views/Home').default
        },

        {
            path: '/eliminados',
            name: 'eliminados',
            component: require('./views/Eliminados').default
        },

        {
            path: '/provincias',
            name: 'provincias',
            component: require('./views/provincias/index').default
        },

        {
            path: '/provincias-eliminadas',
            name: 'provincias-eliminadas',
            component: require('./views/provincias/eliminados').default
        },

        {
            path: '/localidades',
            name: 'localidades',
            component: require('./views/localidades/index').default
        },

        {
            path: '/localidades-eliminadas',
            name: 'localidades-eliminadas',
            component: require('./views/localidades/eliminados').default
        },

        {
            path: '/categorias',
            name: 'categorias',
            component: require('./views/categorias/index').default
        },

        {
            path: '/categorias-eliminadas',
            name: 'categorias-eliminadas',
            component: require('./views/categorias/eliminados').default
        },

        {
            path: '/perfiles',
            name: 'perfiles',
            component: require('./views/perfiles/index').default
        },

        {
            path: '/perfiles-eliminados',
            name: 'perfiles-eliminados',
            component: require('./views/perfiles/eliminados').default
        },
        

        {
            path: '/entiades-eliminados',
            name: 'entidades-eliminados',
            component: require('./views/entidades/eliminados').default
        },

        {
            path: '/entidades-create',
            name: 'entidades-create',
            component: require('./views/entidades/create').default
        },

        {
            path: '/entidades-edit',
            name: 'entidades-edit',
            component: require('./views/entidades/edit').default
        },

        {
            path: '/entidades',
            name: 'entidades',
            component: require('./views/entidades/index').default
        },

        {
            path: '/usuarios-admin',
            name: 'usuarios-admin',
            component: require('./views/usuarios/admin').default
        },

        {
            path: '/usuarios-eliminados',
            name: 'usuarios-eliminados',
            component: require('./views/usuarios/eliminados').default
        },

        {
            path: '/articulos',
            name: 'articulos',
            component: require('./views/articulos/index').default
        },


    ],

	scrollBehavior() {
		return {x:0, y:0}
	}
})