
@component('mail::message')
@if ($user->lang == 'es')
# Hola {{$user->firstname}}, gracias por registrarte.

Por favor confirma tu correo electrónico.
Para ello simplemente debes hacer click en el siguiente boton:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Confirmar mi cuenta
@endcomponent

Muchas gracias,<br>
{{ config('app.name') }}

@else

# Hello {{$user->firstname}}, Thanks for registering.

Please confirm your email.
To do this simply click on the following button:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Confirm my account
@endcomponent
Thank you very much,<br>
{{ config('app.name') }}
@endif

@endcomponent
