<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{asset('assets/lte/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Administración</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('assets/lte/dist/img/login.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->lastname }}, {{ Auth::user()->firstname }}</a>
        </div>
      </div>

      @if(Auth::user()->esSuperAdmin())
         @include("theme.$theme.aside-super")
      @endif

      @if(Auth::user()->esAdmin())
         @include("theme.$theme.aside-admin")
      @endif
    </div>
    <!-- /.sidebar -->
  </aside>