<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>@yield('titulo','MuseoQR')</title>
    
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('assets/lte/plugins/fontawesome-free/css/all.min.css')}}" type="text/css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/lte/dist/css/adminlte.min.css')}}"type="text/css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet" type="text/css">

    @yield('scrips')
  </head>
  <body class="hold-transition sidebar-mini">
    <div class="wrapper">
      @include("theme.lte.aside")
      @include("theme.lte.navbar")
       @include("theme.lte.header")
       
          @yield('tituloContent')
          
          @yield('contenido')
       
      </div>
      @include("theme.lte.footer") 
      
      <!-- ./wrapper -->

      <!-- REQUIRED SCRIPTS -->

      <!-- jQuery -->
      <script src= "{{asset("./assets/lte/plugins/jquery/jquery.min.js")}}" ></script>
      <!-- Bootstrap 4 -->
      <script src= "{{asset("./assets/lte/plugins/bootstrap/js/bootstrap.bundle.min.js")}}" ></script>
      <!-- AdminLTE App -->
      <script src= "{{asset("./assets/lte/dist/js/adminlte.min.js")}}"  ></script>
      
      <script src="{{asset('js/app.js')}}" defer></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

     
      @yield('scrips')
  </body>
</html>