<!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="fa fa-bars" aria-hidden="true"></i>
              <p>
                Parametros
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('paises') }}" class="nav-link">
                  <i class="fa fa-life-ring" aria-hidden="true"></i>
                  <p> Localización</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('categorias') }}" class="nav-link">
                  <i class="fa fa-tasks" aria-hidden="true"></i>
                  <p> Categorias</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{ route('perfiles') }}" class="nav-link">
                  <i class="fa fa-user-secret" aria-hidden="true"></i>
                  <p>Perfiles</p>
                </a>
              </li>
            </ul>
          </li>

          
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>
                  Gestión
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('entidades') }}" class="nav-link">
                    <i class="fa fa-university" aria-hidden="true"></i>
                    <p>Entidades</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="{{ route('usuarios') }}" class="nav-link">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <p>Usuarios</p>
                  </a>
                </li>
                
              </ul>
            </li>

            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <p>
                  Reportes
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('count.artentidad') }}" class="nav-link">
                    <i class="fa fa-industry" aria-hidden="true"></i>
                    <p>Total articulos por Museo</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="{{ route('count.users') }}" class="nav-link">
                    <i class="fa fa-industry" aria-hidden="true"></i>
                    <p>Total de usuarios por Museo</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="{{ route('count.usuarios') }}" class="nav-link">
                    <i class="fa fa-industry" aria-hidden="true"></i>
                    <p>Total de usuarios mensual</p>
                  </a>
                </li>
                
              </ul>
            </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->