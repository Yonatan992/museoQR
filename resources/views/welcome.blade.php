<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="{{asset('img/fav-icon.png')}}" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>MuseoQR</title>

        <!-- Icon css link -->
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/icofont.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/linear-icon/style.css')}}" rel="stylesheet">
        
        <!-- Bootstrap -->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="{{asset('vendors/revolution/css/settings.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/revolution/css/layers.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/revolution/css/navigation.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/animate-css/animate.css')}}" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="{{asset('vendors/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/owl-carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/swiper/css/swiper.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/flipster-slider/jquery.flipster.min.css')}}" rel="stylesheet">
        
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
        <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
         
    </head>
    <body data-spy="scroll" data-target="#bs-example-navbar-collapse-1" data-offset="100">
       
       <div id="preloader">
            <div id="preloader_spinner">
				<div class="pre_inner">
					<div class="dot dot-1"></div>
					<div class="dot dot-2"></div>
					<div class="dot dot-3"></div>
				</div>
            </div>
        </div>
       
        <!--================Header Area =================-->
        <header class="dash_tp_menu_area nine_menu">
            <div class="container">
            	<nav class="navbar navbar-default">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li class="active"><a href="#about">Quienes Somos</a></li>
							<li><a href="#feature">Características</a></li>
							<li><a href="#team">Museos</a></li>
							<li><a href="#screenshot">Capturas</a></li>
							<li><a href="#price">Prueba aquí</a></li>
                            <li><a href="{{ url('/en') }}"><img src="{{asset('img/landing/reino-unido.png')}}"/> English</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</nav>
            </div>
        </header>
        <!--================End Header Area =================-->
        
        <!--================End Header Area =================-->
        <section class="slider_two_area" id="home">
            <div id="main_slider_ten" class="rev_slider" data-version="5.3.1.6">
                <ul> 
                    <li data-index="rs-2972" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="{{asset('img/landing/home-slider/slider-1.jpg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Web Show" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <!-- LAYERS -->
                        <div class="slider_text_box">
                            <div class="tp-caption first_text" 
                            data-x="['right','right','center','center']" 
                            data-y="['middle','middle','middle','middle']" 
                            data-hoffset="['20','20','0','0']" 
                            data-voffset="['-100','-100','-100','-100']" 
                            data-fontsize="['20','20','20','20','20']" 
                            data-lineheight="['30','30','30','30','30']"
                            data-width="['none','none','none','100%','100%']"
                            data-height="none"
                            data-whitespace="['nowrap','nowrap','nowrap','normal']"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1700,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"x:left(R);","ease":"Power3.easeIn"}]'
                            data-textAlign="['right','right','right','center']">
                            </div>
                            
                            <div class="tp-caption secand_text" 
                            data-x="['right','right','center','center']" 
                            data-y="['middle','middle','middle','middle']" 
                            data-hoffset="['25','36','0','0']" 
                            data-voffset="['-10','0','0','0','-20']"  
                            data-fontsize="['60','40','40','40','30']"
                            data-lineheight="['60','50','50','50','40']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1750,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                            data-textAlign="['right','right','center','center','center']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]">Bienvenidos<br> a la nueva forma <br>de recorrer un museo</div>
                            
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-2 ten_mobile" 
                                data-x="['-50','-50','-50','-50']" 
                                data-hoffset="['0','0','0','0']" 
                                data-y="['bottom','bottom','bottom','bottom']" 
                                data-voffset="['0','0','0','0']" 
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="image" 
                                data-responsive_offset="on" 
                                data-frames='[{"from":"x:right(R);","speed":800,"to":"o:1;","delay":750,"ease":"Power4.easeOut"},{"delay":"wait","speed":800,"to":"x:left(R);","ease":"Power4.easeIn"}]'
                                data-textAlign="['left','left','left','left']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style="z-index: 5;"><img src="{{asset('img/landing/home-slider/scanner.png')}}" alt="" data-no-retina> 
                            </div>
                        </div>
                    </li>
                    <li data-index="rs-2973" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="{{asset('img/landing/home-slider/slider-1.jpg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Web Show" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <!-- LAYERS -->
                        <div class="slider_text_box">
                            <div class="tp-caption first_text" 
                            data-x="['right','right','center','center']" 
                            data-y="['middle','middle','middle','middle']" 
                            data-hoffset="['20','20','0','0']" 
                            data-voffset="['-100','-100','-100','-100']" 
                            data-fontsize="['20','20','20','20','20']" 
                            data-lineheight="['30','30','30','30','30']"
                            data-width="['none','none','none','100%','100%']"
                            data-height="none"
                            data-whitespace="['nowrap','nowrap','nowrap','normal']"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1700,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"x:left(R);","ease":"Power3.easeIn"}]'
                            data-textAlign="['right','right','right','center']">
                            </div>
                            
                            <div class="tp-caption secand_text" 
                            data-x="['right','right','center','center']" 
                            data-y="['middle','middle','middle','middle']" 
                            data-hoffset="['25','36','0','0']" 
                            data-voffset="['-10','0','0','0','-20']"  
                            data-fontsize="['60','40','40','40','30']"
                            data-lineheight="['60','50','50','50','40']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1750,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                            data-textAlign="['right','right','center','center','center']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]">Sea parte<br>de una nueva<br>experiencia</div>
                            
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-2 ten_mobile" 
                                data-x="['-50','-50','-50','-50']" 
                                data-hoffset="['0','0','0','0']" 
                                data-y="['bottom','bottom','bottom','bottom']" 
                                data-voffset="['0','0','0','0']" 
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="image" 
                                data-responsive_offset="on" 
                                data-frames='[{"from":"x:right(R);","speed":800,"to":"o:1;","delay":750,"ease":"Power4.easeOut"},{"delay":"wait","speed":800,"to":"x:left(R);","ease":"Power4.easeIn"}]'
                                data-textAlign="['left','left','left','left']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style="z-index: 5;"><img src="{{asset('img/landing/home-slider/scanner.png')}}" alt="" data-no-retina> 
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!--================End Header Area =================-->
        
        <!--================Ten Registration Area =================-->
        <section class="ten_registration">
        	<div class="container">
        		
        		<div class="pull-right">
        		</div>
        	</div>
        </section>
        <!--================End Ten Registration Area =================-->
        
        <!--================Powerfull Area =================-->
        <section class="ten_powerfull_area p_100" id="about"> 
        	<div class="container">
        		<div class="ten_title">
        			<h2>Sobre <span>nosotros</span></h2>
        			
        		</div>
        		<div class="powerfull_inner">
        			<div class="row">
        				<div class="col-md-3 col-xs-6">
        					<div class="pw_item">
        						<i class="icofont icofont-repair"></i>
        						<h4>Seguridad</h4>
        					</div>
        				</div>
        				<div class="col-md-3 col-xs-6">
        					<div class="pw_item">
        						<i class="icofont icofont-speed-meter"></i>
        						<h4>Interfaz agradable</h4>
        					</div>
        				</div>
        				<div class="col-md-3 col-xs-6">
        					<div class="pw_item">
        						<i class="icofont icofont-light-bulb"></i>
        						<h4>Funciones seguras</h4>
        					</div>
        				</div>
        				<div class="col-md-3 col-xs-6">
        					<div class="pw_item">
        						<i class="icofont icofont-bolt"></i>
        						<h4>Confiable</h4>
        					</div>
        				</div>
        			</div>
        		</div>
        		<div class="ten_perfomance">
        			<div class="row">
        				<div class="col-md-6">
        					<div class="ten_p_img">
        						<img src="{{asset('img/landing/slide-0.jpg')}}" alt="">
        					</div>
        				</div>
        				<div class="col-md-6">
        					<div class="ten_p_text">
        						
        						<p>Soy Licenciado en Sistemas de Información, entusiasta con ánimo de emprender, adquirir nuevas habilidades para crear soluciones tecnológicas que faciliten la vida cotidiana de las personas. La idea surgió con la propuesta en común de un compañero al observar que cuando visitaba una entidad (ejemplo: museos) no contaban con herramientas que les permita explorar con mayor detalle el artículo exhibido.
                                La aplicación como servicio brinda la posibilidad de poder acceder a una colección digital de artículos publicados por museos que quieran hacer visible lo que ofrecen. Otro servicio es el de poder escanear un código QR que va a estar asociado a un artículo, permitiendo acceder a mayor información, donde podrá dejar una valoración seguido de una reseña.
                                </p>
        						
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Powerfull Area =================-->
        
        <!--================Ten Discover Area =================-->
        <section class="ten_discover_area" id="feature">
        	<div class="container">
        		<div class="ten_d_text">
        			<h2>Descubra las funciones de nuestra aplicación</h2>
        			
        		</div>
        		<div class="ten_discover_inner">
        			<div class="row">
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-1.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Catalogos</h4>
        							<p>Explorá la colección digital con más de 2500 artículos publicados por los museos adheridos.</p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-2.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Multimedia</h4>
        							<p>Encontrarás imágenes en alta definición, textos y comentarios de los artículos.</p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-3.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Listados</h4>
        							<p>Explorá los artículos, categorías y mejores valorados por museos. <br><br></p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-4.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Ubicación</h4>
        							<p>Explorá la opcion de "Como llegar" para ver el recorrido de su ubicacion actual a la entidad elegida.</p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-5.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Lenguajes</h4>
        							<p>Visualiza el contenido de la aplicación en Castellano e Ingles</p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-6.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Scanner</h4>
        							<p> REGÍSTRESE AHORA en la APP de forma muy sencilla y gratuita y acceda al servicio Scanner y a mas funciones.</p>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Ten Discover Area =================-->
        <!--================Ten Team Area =================-->
        <section class="ten_team_area p_100" id="team">
        	<div class="container">
        		<div class="ten_title">
					<h2><span>Museos que nos eligen</span></h2>
        		</div>
        		<div class="ten_team_slider owl-carousel">
                    @foreach($entidades as $entidad)
        			<div class="item">
        				<div class="ten_team_item">
        					<img src="{{asset('img/'.$entidad->portada)}}" class="rounded mx-auto d-block" width="200" height="200" alt="">
        					<h4>{{$entidad->nombre}}</h4>
        					<p>Imagen ilustrativa</p>
        					<div class="ten_team_hover">
        						<div class="ten_team_h_inner">
        							<h4>{{$entidad->nombre}}</h4>
        							<h5>{{$entidad->direccion}}</h5>
        							<h6>Redes Sociales</h6>
        							<ul>
                                        <li><a href="{{$entidad->pagina}}"><i class="fa fa-product-hunt"></i></a></li>
        								<li><a href="{{$entidad->facebook}}"><i class="fa fa-facebook"></i></a></li>
        								<li><a href="{{$entidad->instagram}}"><i class="fa fa-instagram"></i></a></li>
        								<li><a href="{{$entidad->twitter}}"><i class="fa fa-twitter"></i></a></li>
        								
        							</ul>
        						</div>
        					</div>
        				</div>
        			</div>
        		@endforeach
        		</div>
        	</div>
        </section>
        <!--================End Ten Team Area =================-->
        <!--================Ten App Screen Area =================-->
        <section class="ten_screen_area" id="screenshot">
        	<div class="container">
        		<div class="ten_d_text">
					<h2><span>Capturas</span> de pantallas</h2>
        		</div>
        		<div class="flip_ten_slider">
					<ul class="flip-items">
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/principal2.jpeg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/lenguaje2.jpeg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/menu2.jpeg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/menuBarra2.jpeg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/scanEs.jpeg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/categorias2.jpeg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/mejVal2.jpeg')}}"  alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/listMuseos.jpeg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/artScaneado2.jpeg')}}" alt="">
						</li>
                        <li data-flip-title="Red">
                            <img src="{{asset('img/landing/3d-slider/artScaneado3.jpeg')}}" alt="">
                        </li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Ten App Screen Area =================-->       
        
        <!--================Ten Price Area =================-->
        <section class="ten_screen_area" id="price">
        	<div class="container">
                <div class="ten_d_text">
                    <h2><span>Codigos QR</span> de prueba</h2>
                    <p>Imágenes ilustrativas</p>
                </div>
        		<div class="row ten_price_list_inner">
        			<div class="col-md-4 col-xs-6">
                        <div class="ten_price_item blue">
                            <div class="ten_price_item_box">
                                <div class="ten_p_head">
                                    <h3>El jaguar, yaguar o yaguareté</h3>
                                    <h5>Museo de Ciencias Naturales <br> "Amado Bonpland"</h5>
                                </div>
                                <ul class="text-center">
                                    <li data-flip-title="Red">
                                        <img src="{{asset('img/landing/codigosQR/codigo1.jpeg')}}" alt="">
                                    </li>
                                </ul>
                            </div>
                            <p class="ten_p_btn"></p>
                        </div>
                    </div>
        			<div class="col-md-4 col-xs-6">
        				<div class="ten_price_item blue">
							<div class="ten_price_item_box">
								<div class="ten_p_head">
									<h3>Barcos medievales</h3>
                                    <h5>Museo Historico de Vias Navegables <br><br></h5>
								</div>
								<ul class="text-center">
									<li data-flip-title="Red">
                                        <img src="{{asset('img/landing/codigosQR/codigo2.jpeg')}}" alt="">
                                    </li>
								</ul>
							</div>
       						<p class="ten_p_btn"></p>
        				</div>
        			</div>
        			<div class="col-md-4 col-xs-6">
                        <div class="ten_price_item blue">
                            <div class="ten_price_item_box">
                                <div class="ten_p_head">
                                    <h3>La noche estrellada</h3>
                                    <h5>Museo Provincial de Bellas Artes <br>"Dr. Juan R. Vidal"</h5>
                                </div>
                                <ul class="text-center">
                                    <li data-flip-title="Red">
                                        <img src="{{asset('img/landing/codigosQR/codigo4.jpeg')}}" alt="">
                                    </li>
                                </ul>
                            </div>
                            <p class="ten_p_btn"></p>
                        </div>
                    </div>
        		</div>
        	</div>
        </section>
        <!--================End Ten Price Area =================-->
        
        <!--================Ten Question Area =================-->
        <section class="ten_question_area">
        	<div class="container">
        		<div class="ten_title">
					<h2>Cinco preguntas <span>importantes</span> sobre la aplicación de Museo QR</h2>
        			
        		</div>
        		<div class="tex_ques_inner">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									¿cómo obtengo la Aplicación Museo QR?
									<i class="lnr lnr-circle-minus"></i>
									<i class="lnr lnr-plus-circle"></i>
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
								Primero, abre la tienda de aplicaciones de tu celular, luego selecciona buscar, después escribe Museo QR y selecciona en la lista. Por último ingresa y registrate en la aplicación.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									¿Como registrarse en la Aplicación?
									<i class="lnr lnr-circle-minus"></i>
									<i class="lnr lnr-plus-circle"></i>
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
								Registrarse es muy sencillo, completa con tus datos en la sección Registrarse de la aplicación y se le enviará un mensaje a su email para la verificación del mismo. Una vez recibido el email acceder al link que se le adjunta y listo.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									¿Cómo debo usar el scanner de la Aplicación?
									<i class="lnr lnr-circle-minus"></i>
									<i class="lnr lnr-plus-circle"></i>
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body">
								Para utilizar el escáner debe estar primero que nada registrado y verificado por el sistema, si cumple con estas condiciones una vez que esté en el museo en frente del código QR solo es cuestión de dirigirse a la sección Scan de la aplicación y escanea el código QR.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingfour">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
									¿Cómo dejar una valoración y una reseña a un articulo escaneado?
									<i class="lnr lnr-circle-minus"></i>
									<i class="lnr lnr-plus-circle"></i>
									</a>
								</h4>
							</div>
							<div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
								<div class="panel-body">
								Una vez escaneado el código QR el mismo le devolverá toda la información de este artículo, con la posibilidad de dejar una valoración seguido de una reseña, el mismo solo tendrá un máximo de 45 minutos para editarlo y solo podrá dejar una reseña a este artículo escaneado.
								</div>
							</div>
						</div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingfive">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                    ¿Como usar el idioma inglés?
                                    <i class="lnr lnr-circle-minus"></i>
                                    <i class="lnr lnr-plus-circle"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                <div class="panel-body">
                                Al iniciar la aplicación ya nos da la opción de optar por Español E Inglés si eligiéramos la sección Inglés, todo el contenido se visualizará en tal idioma,al registrarse, el email que se le enviará estaría en dicho lenguaje, al igual que la recuperación de contraseña.
                                </div>
                            </div>
                        </div>
					</div>
        		</div>
        	</div>
        </section>
        <!--================End Ten Question Area =================-->
        
        <!--================Footer Area =================-->
        <footer class="ten_footer_area">
            <div class="ten_footer_widget">
            	<div class="container">
            		<div class="row">
            			<div class="col-md-5 col-xs-6">
            				<aside class="t_f_widget t_ab_widget"> 
            					<img src="{{asset('img/ten-f-logo.png')}}" alt="">
            					<h4>Nosotros</h4>
            					<p> MuseoQR es un sistema web y móvil pensado para la publicación de artículos de un museo con el fin de motivar las visitas a las entidades.</p>
            				</aside>
            			</div>
            			<div class="col-md-3 col-xs-6">
            				<aside class="t_f_widget t_link_widget"> 
            					<div class="ten_f_title">
            						<h3>Enlace de referencia</h3>
            					</div>
            					<ul>
            						<li><a href="#"><i class="icofont icofont-thin-right"></i> Inicio</a></li>
            						<li><a href="#about"><i class="icofont icofont-thin-right"></i> Quienes somos</a></li>
            						<li><a href="#feature"><i class="icofont icofont-thin-right"></i> Características</a></li>
            						<li><a href="#screenshot"><i class="icofont icofont-thin-right"></i> Capturas</a></li>
            						<li><a href="#price"><i class="icofont icofont-thin-right"></i> Prueba aquí</a></li>
            					</ul>
            				</aside>
            			</div>
            			<div class="col-md-4 col-xs-12">
            				<aside class="t_f_widget t_contact_widget"> 
            					<div class="ten_f_title">
            						<h3>Contacto</h3>
            					</div>
            					<h4>Dirección</h4>
            					<h5><i class="fa fa-phone"></i>  379- 4895599</h5>
            					<p><i class="fa fa-envelope"></i> infomuseo2020@gmail.com</p>
             					<a class="send_bent" href="#">Enviar mensaje</a>
            					
            				</aside>
            			</div>
            		</div>
            	</div>
            </div>
            <div class="ten_f_copyright">
            	<div class="container">
            		<p class="copyright">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a><br>
<a href="#" target="_blank">Terminos de Uso Política de Privacidad </a>

<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
            	</div>
            </div>
        </footer>
        <!--================End Footer Area =================-->
        
        
        
        
        
        
        
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{{asset('js/jquery-2.2.4.js')}}"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <!-- Rev slider js -->
        <script src="{{asset('vendors/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
        <!-- Extra Plugin -->
        <script src="{{asset('vendors/parallax/jquery.parallax-scroll.js')}}"></script>
        <script src="{{asset('vendors/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{asset('vendors/counterup/waypoints.min.js')}}"></script>
        <script src="{{asset('vendors/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('vendors/isotope/imagesloaded.pkgd.min.js')}}"></script>
        <script src="{{asset('vendors/isotope/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('vendors/flexslider/flex-slider.js')}}"></script>
        <script src="{{asset('vendors/flexslider/mixitup.js')}}"></script>
        <script src="{{asset('vendors/nice-selector/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('vendors/swiper/js/swiper.min.js')}}"></script>
        <script src="{{asset('vendors/flipster-slider/jquery.flipster.min.js')}}"></script>
        
        <!--gmaps Js-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
        <script src="{{asset('js/gmaps.min.js')}}"></script>
        
        <script src="{{asset('js/theme.js')}}"></script>
    </body>
</html>
