@extends("theme.$theme.layout")

@section('titulo')
Reportes 
@endsection

@section('contenido')
<div class="container">
    @if (empty($ent))
          <div class="alert alert-warning">
            <h3 align="center">No hay registros</h3>

          </div>
     @else
    {!! $ent->html() !!}
    @endif
                
</div>
{!! Charts::scripts() !!}
@if (!empty($ent))
		{!! $ent->script()!!}
@endif
@endsection
