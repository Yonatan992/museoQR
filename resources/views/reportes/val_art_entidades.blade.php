@extends("theme.$theme.layout")

@section('titulo')
Reportes 
@endsection

@section('contenido')

<div class="container" id="chart-container">
	<br>
	@if (empty($valoraciones))
          <div class="alert alert-warning">
            <h3 align="center">No hay registros</h3>

          </div>
     @else

	{!! $valoraciones->html() !!}
	@endif

</div>
{!! Charts::scripts() !!}
@if (!empty($valoraciones))
		{!! $valoraciones->script()!!}
@endif

@endsection