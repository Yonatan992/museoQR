@extends("theme.$theme.layout")

@section('titulo')
Reportes 
@endsection

@section('contenido')
<div class="container">
    @if (empty($users))
          <div class="alert alert-warning">
            <h3 align="center">No hay registros</h3>

          </div>
     @else
    {!! $users->html() !!}
    @endif
                
</div>
{!! Charts::scripts() !!}
@if (!empty($users))
		{!! $users->script()!!}
@endif
@endsection