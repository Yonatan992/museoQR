@extends("theme.$theme.layout")

@section('titulo')
Reportes 
@endsection

@section('contenido')
<div class="container">
    @if (empty($usuarios))
          <div class="alert alert-warning">
            <h3 align="center">No hay registros</h3>

          </div>
     @else
    {!! $usuarios->html() !!}
    @endif
                
</div>
{!! Charts::scripts() !!}
@if (!empty($usuarios))
		{!! $usuarios->script()!!}
@endif
@endsection