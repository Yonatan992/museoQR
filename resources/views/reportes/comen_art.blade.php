@extends("theme.$theme.layout")

@section('titulo')
Reportes 
@endsection

@section('contenido')
<div class="container">
    @if (empty($comentarios))
          <div class="alert alert-warning">
            <h3 align="center">No hay registros</h3>

          </div>
     @else
    {!! $comentarios->html() !!}
    @endif
                
</div>
{!! Charts::scripts() !!}
	@if (!empty($comentarios))
		{!! $comentarios->script()!!}
	@endif
@endsection
