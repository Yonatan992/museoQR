@extends("theme.$theme.layout")

@section('titulo')
articulos-eliminados   
@endsection

@section('contenido')
<div class="container">
	@if(Session::has('message'))
 <div class="alert alert-success" role="alert">
  {{ Session::get('message') }}
</div>
@endif

@if (session('info'))
<div class="alert alert-info" role="alert">
  {{ session('info') }}
</div>
@endif
<div class="row mt-5">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Lista de usuarios registrados</h3>
        <div class="card-tools">
          <a href="{{url('admin/articulos')}}" class="btn btn-success" data-target="#addNew" @click="openModalWindow">Volver <i class="fas fa-reply-all fa-fw"></i></a>
        </div>
      </div>
      <div class="card-body table-responsive p-0">
        @if (count($articulos) == 0)
          <div class="alert alert-warning">
            <h3 align="center">No hay elementos eliminados</h3>

          </div>
        @else
        <table class="table table-hover">
          <tbody>
            <tr>
              <th>Título</th>
              <th>Estado</th>
              <th>Categoria</th>
              <th>Imagen</th>
              <th>QR</th>
              <th>Restaurar</th>
            </tr>
            @foreach($articulos as $articulo)
            <tr>
              <td>{{ $articulo->titulo }}</td>
              @if ($articulo->status == 'PUBLISHED')
              <td> Publicado</td>
              @else
              <td>Borrador</td>
              @endif
              <td>{{ $articulo->category->nombre }}</td>
              <td> <img  src="{{asset('img/'.$articulo->portada) }}" class="card imgModal" width="100" height="100" alt="Imagen no Disponible" /></td>
            </td>

            <td> <img  src="{{asset('img/'.$articulo->codeQr) }}" class="card imgModal" width="100" height="100" alt="Imagen no Disponible" /></td>
          </td>

          <td>
            <a href="{{url('articulo-restaurar/'.$articulo->id)}}">
              <i class="fa fa-window-restore blue"></i>
            </a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{ $articulos->links() }}
    @endif
  </div>
</div>
</div>
</div>
</div>
@endsection

