@extends("theme.$theme.layout")

@section('header')
<div class="container">
  <div class="card text-white bg-info col-md-12">
    
    <h3>Administracion de la entidad</h3>
    <h4>{{Auth::user()->entity->nombre}}</h4>

  </div>
  
</div>

@endsection

@section('titulo')
crear-articulo 
@endsection

@section('contenido')
<div class="container">
  @if(Session::has('message'))
  <div class="alert alert-success" role="alert">
    {{ Session::get('message') }}
  </div>
  @endif
  <br>

  <div class="card">
   <!--  Cabecera Card -->
   <div class="card-header">
    Crear artículo
  </div>
  <div class="card-body">
    <form action="{{route('store.articulo')}}" method="POST" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="entity_id" value="{{auth()->user()->entity_id }}" >
      <center>
        <div class="form-group">
          <div class="col-md-12 text-center">
            <a type=button href="{{url('admin/articulos')}}" class="btn btn-outline-secondary btn-lg"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Volver</a>&nbsp;&nbsp;
            <button type="submit" class="btn btn-outline-primary btn-lg">Publicar&nbsp;&nbsp;<i class="fa fa-check-square-o" aria-hidden="true"></i></button>
          </div>
        </div>
      </center>
      <div class="card col-md-6">
        <div class="card-header">
          Imagen del artículo
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="portada" class="labelArticle">Subir im&#225;gen</label>
            <input type="file" name="portada" accept="imagen/*" class="form-control-file" id="portada">
            <span class="text-danger">{{ $errors->first('portada') }}</span>
          </div>
        </div>
      </div>
      <div id="accordion">
        <div class="card">
          <div class="card-header">
            <h5 class="mb-0">
              <a class="card-link" data-toggle="collapse" href="#collapseOne">
                Parametros del artículo
              </a>
            </h5>
          </div>
          <div id="collapseOne" class="collapse show" data-parent="#accordion">
            <div class="card-body">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="categoria">Seleccionar Categoria</label>
                  <select name="category_id">
                    <option value="" selected disabled>seleccione la categoria</option>
                    @foreach($categorias as $categoria)
                    <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                    @endforeach

                  </select>
                  <br>
                  <span class="text-danger">{{ $errors->first('category_id') }}</span>
                </div>

                <div class="form-group col-md-6">
                  <label for="valor" class="labelArticle"> Estado del artículo: </label>
                  <br>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="status" value="PUBLISHED" @if(old('status') == 'PUBLISHED') checked @endif >
                    <label class="form-check-label" for="status" style="color:green;">Publicado</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="status" value="DRAFT"@if(old('status') == 'DRAFT') checked @endif >
                    <label class="form-check-label" for="status" style="color:red;">Borrador</label>
                  </div>
                  <br>
                  <span class="text-danger">{{ $errors->first('status') }}</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h5 class="mb-0">
              <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                Cabecera del artículo
              </a>
            </h5>
          </div>
          <div id="collapseTwo" class="collapse" data-parent="#accordion">
            <div class="card-body">
              <div class="row">
                <div class="form-group col-md-6">
                  <label>Titulo en Español</label>
                  <h6>Maximo <span class="label label-info">100 caracteres</span>(*)</h6>
                  <textarea class="form-control" name="titulo" id="titulo" placeholder="Ingrese título del artículo" rows="2" maxlength="100">{{old('titulo')}}</textarea>
                  <span class="text-danger">{{ $errors->first('titulo') }}</span>
                </div>
                <div class="form-group col-md-6">
                  <label>Titulo en Ingles</label>
                  <h6>Maximo <span class="label label-info">100 caracteres</span>(*)</h6>
                  <textarea class="form-control" name="title" id="title" placeholder="Ingrese título en ingles del artículo" rows="2" maxlength="100">{{old('title')}}</textarea>
                  <span class="text-danger">{{ $errors->first('title') }}</span>
                </div>

                <div class="form-group col-md-6">
                  <label>Subtitulo en Español</label>
                  <h6>Maximo <span class="label label-info">100 caracteres</span>(*)</h6>
                  <textarea class="form-control" name="subtitulo" id="subtitulo" placeholder="Ingrese título del artículo" rows="2" maxlength="100">
                    {{old('subtitulo')}}
                  </textarea>
                  <span class="text-danger">{{ $errors->first('subtitulo') }}</span>
                </div>

                <div class="form-group col-md-6">
                  <label>Subtitulo en Ingles</label>
                  <h6>Maximo <span class="label label-info">100 caracteres</span>(*)</h6>
                  <textarea class="form-control" name="subtitle" id="subtitle" placeholder="Ingrese título del artículo" rows="2" maxlength="100">
                    {{old('subtitle')}}
                  </textarea>
                  <span class="text-danger">{{ $errors->first('subtitle') }}</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h5 class="mb-0">
              <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                Cuerpo del artículo
              </a>
            </h5>
          </div>
          <div id="collapseThree" class="collapse" data-parent="#accordion">
            <div class="card-body">
              <div class="row">
                <div class="form-group col-md-6">
                  <label>Breve descripción en Español (*)</label>
                  <textarea class="form-control" name="descripcion" id="descripcion" placeholder="Ingrese título del artículo" rows="2" maxlength="100">
                    {{old('descripcion')}}
                  </textarea>
                  <span class="text-danger">{{ $errors->first('descripcion') }}</span>
                </div>
                <div class="form-group col-md-6">
                  <label>Breve descripción en Ingles (*)</label>
                  <textarea class="form-control" name="description" id="description" placeholder="Ingrese título en ingles del artículo" rows="2" maxlength="100">
                    {{old('description')}}
                  </textarea>
                  <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>

                <div class="form-group col-md-6">
                  <label>Cuerpo en Español (*)</label>
                  <textarea class="form-control" name="cuerpo" id="cuerpo" placeholder="Ingrese título del artículo" rows="3" maxlength="100">
                    {{old('cuerpo')}}
                  </textarea>
                  <span class="text-danger">{{ $errors->first('cuerpo') }}</span>
                </div>

                <div class="form-group col-md-6">
                  <label>Cuerpo en Ingles (*)</label>
                  <textarea class="form-control" name="body" id="body" placeholder="Ingrese título del artículo" rows="3">
                    {{old('body')}}
                  </textarea>
                  <span class="text-danger">{{ $errors->first('body') }}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>




</div>

</div>

<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script>
  CKEDITOR.replace( 'description');
  CKEDITOR.replace( 'descripcion');
  CKEDITOR.replace( 'cuerpo' );
  CKEDITOR.replace( 'body' );
</script>
@endsection