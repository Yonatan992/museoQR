@extends("theme.$theme.layout")

@section('header')
<div class="container">
  <div class="card text-white bg-info col-md-12">

    <h3>Administracion de la entidad</h3>
    <h4>{{Auth::user()->entity->nombre}}</h4>

  </div>
  
</div>

@endsection

@section('titulo')
articulo   
@endsection

@section('contenido')
<div class="container">
	@if(Session::has('message'))
 <div class="alert alert-success" role="alert">
  {{ Session::get('message') }}
</div>
@endif

@if (session('info'))
<div class="alert alert-info" role="alert">
  {{ session('info') }}
</div>
@endif
<div class="row mt-5">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Lista de artículos</h3>
        <br>
        <div class="card-tools">
          <a href="{{url('create-articulo')}}" class="btn btn-success">Agregar Nuevo <i class="fas fa-plus fa-fw"></i></a>

          <a href="{{url('articulos-eliminados/'.Auth::user()->entity_id)}}" class="btn btn-danger">Eliminados <i class="fas fa-trash fa-fw"></i></a>

        </div>
      </div>
      <div class="card-body table-responsive p-0">
         @if (count($articulos) == 0)
          <div class="alert alert-warning">
            <h3 align="center">No hay artículos publicados</h3>

          </div>
        @else
        <table class="table table-hover">
          <tbody>
            <tr>
              <th>Título</th>
              <th>Estado</th>
              <th>Categoria</th>
              <th>Imagen</th>
              <th>QR</th>
              <th>Modificar</th>
              <th>Comentarios</th>
              <th>Imprimir QR</th>
            </tr>
            @foreach($articulos as $articulo)
            <tr>
              <td>{{ $articulo->titulo }}</td>
              @if ($articulo->status == 'PUBLISHED')
              <td> Publicado</td>
              @else
              <td>Borrador</td>
              @endif
              <td>{{ $articulo->category->nombre }}</td>
              <td> <img  src="{{asset('img/'.$articulo->portada) }}" class="card imgModal" width="100" height="100" alt="Imagen no Disponible" /></td>
            </td>

            <td> <img  src="{{asset('img/'.$articulo->codeQr) }}" class="card imgModal" width="100" height="100" alt="Imagen no Disponible" /></td>
          </td>

          <td>
            <a href="{{url('edit-articulo/'.$articulo->id)}}">
              <i class="fa fa-edit blue"></i>
            </a>
            |
            <a href="#delete{{$articulo->id}}"  data-original-title="Eliminar Articulo"  title="Eliminar" class="btn  btn-danger btn-sm " data-toggle="modal" role ="button">
              <i class="fa fa-trash red"></i>
            </a>
            </td>
            <td>
            <a href="{{url('comentarios/'.$articulo->id)}}">
              <i class="fa fa-comments blue"></i>
            </a>
          </td>
            <td>
            <a href="{{url('imprimir/'.$articulo->id)}}">
              <i class="fa fa-download blue"></i>
            </a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{ $articulos->links() }}
    @endif
  </div>
</div>
</div>
</div>
</div>


@foreach($articulos as $articulo)
<div class="modal fade" id="delete{{ $articulo->id }}" tabindex="-1" role="dialog" aria-labelledby="eliminar" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-headerArticle">
        <h4 class="alert alert-danger" style="background-color: #dc3545;; color: #FFFAFA; ">Eliminar artículo
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-times" aria-hidden="true"></span></button></h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-info"> Está seguro que desea ELIMINAR el Articulo: {{$articulo->titulo}}, Categoria: {{$articulo->category->nombre}}</div>
        </div>
        <div class="modal-footer ">
          <form method="POST" action="{{ route('delete.articulo',['id' => $articulo->id]) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            @csrf
            <button  class="btn btn-danger"  type="submit"><span class="fa fa-ok-sign"></span>Si</button>
          </form>
          <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-remove"></span> No</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  @endforeach
  @endsection

