<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>MUseoQR</title>
    <!-- Scripts -->
    <script src="{{ public_path('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

    <style>
        h1{
            text-align: center;
            text-transform: uppercase;
        }
        .contenido{
            font-size: 20px;
        }
        #primero{
            background-color: #ccc;
        }
        #segundo{
            color:#44a359;
        }
        #tercero{
            text-decoration:line-through;
        }
    </style>
</head>
<body>
        <div class="card">
            <div class="card-header">
                <h1>{{$articulo->titulo}}</h1>
                <h4>{{$articulo->entity->nombre}}</h4>
                <hr>
            </div>
        </div>
        <br> <br>
        <div class="card">
            <div class="card-header">
                <hr>
                <h3 align="center">Escanea y acceda a la información del artículo</h3>
                <hr>
            </div>
            
        </div>

        <div class="container">
            <img src="{{public_path('img/'.$articulo->codeQr) }}" width="500" height="500" alt="Imagen no Disponible" />
        </div>
</body>
</html>