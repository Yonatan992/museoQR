@extends("theme.$theme.layout")

@section('header')
<div class="container">
  <div class="card text-white bg-info col-md-12">

    <h3>Administracion de la entidad</h3>
    <h4>{{Auth::user()->entity->nombre}}</h4>

  </div>
  
</div>

@endsection

@section('titulo')
articulo   
@endsection

@section('contenido')

<div class="container">
  @if(Session::has('message'))
  <div class="alert alert-success" role="alert">
    {{ Session::get('message') }}
  </div>
  @endif
  <br>

  @if (session('info'))
    <div class="alert alert-success" role="alert">
      {{ session('info') }}
    </div>
  @endif

<div class="container">

  <div class="card">
    <div class="card-header">
      <i class="fa fa-puzzle-piece" aria-hidden="true"></i>  {{$articulo->titulo}}
      <div class= "container">
        <img  src="{{asset('img/'.$articulo->portada) }}" width="400" height="200" class="rounded mx-auto d-block"alt="Imagen no Disponible" />
      </div>
      <hr>
      <h4><i class="fa fa-comments" aria-hidden="true"></i>  Comentarios</h4>
    </div>
   
    @if(count($articulo->comments) > 0)
      @foreach($articulo->comments as $comment)
        <ul class="list-group list-group-flush">
          <li class="list-group-item"> 
            <i class="fa fa-user"></i> {{$comment->user->firstname}},  {{$comment->user->lastname}}
          <br>
          <a href="#delete{{$comment->id}}"  data-original-title="Eliminar Comentario"  title="Eliminar" class="btn  btn-danger btn-sm float-right" data-toggle="modal" role ="button">
              <i class="fa fa-trash red"></i>
          </a>
          <br><i class="fa fa-comment"></i> {{$comment->descripcion}}</li>
        </ul>
      @endforeach
    @else
      <div class="container">
        <h3 align="center">No hay comentarios</h3>
              
      </div>
    @endif

  </div>
	
</div>

@foreach($articulo->comments as $comment)
<div class="modal fade" id="delete{{ $comment->id }}" tabindex="-1" role="dialog" aria-labelledby="eliminar" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <h4 class="alert alert-light">Eliminar comentario
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-times" aria-hidden="true"></span></button></h4>
        <div class="modal-body">
          <div class="alert alert-success" role="alert"> Está seguro que desea ELIMINAR el Comentario del usuario:  {{$comment->user->firstname}}, {{$comment->user->lastname}}</div>
        </div>
        <div class="modal-footer ">
          <form method="POST" action="{{ url('comentarios/'.$comment->id) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            @csrf
            <button  class="btn btn-danger"  type="submit"><span class="fa fa-ok-sign"></span>Si</button>
          </form>
          <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-remove"></span> No</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

@endforeach
@endsection

