<div class="modal fade" id="eliminados">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
				<h4>Lista paises eliminados</h4>
			</div>
            <br>
			<div class="modal-body">

            <table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>Nombre español</th>
                    <th>Nombre ingles</th>
					<th colspan="2">
						&nbsp;
					</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="country in countriesDelete">
					<td>@{{ country.nombreEs }}</td>
                    <td>@{{ country.nombreEn }}</td>
					<td width="10px">
						<a href="#" type="button" class="btn btn-outline-success" v-on:click.prevent="restoreCountry(country)">Restaurar</a>
					</td>
            
				</tr>
			</tbody>
		</table>

			</div>
		</div>
	</div>
</div>