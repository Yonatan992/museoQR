<form method="POST" v-on:submit.prevent="createCountry">
<div class="modal fade" id="create">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
				<h4>Nuevo Pais</h4>
			</div>
			<div class="modal-body">
				<label for="nombreEs">Nombre</label>
				<input type="text" name="nombreEs" class="form-control" v-model="nombreEs">
				<span v-for="error in errors" class="text-danger">@{{ error.nombreEs }}</span>
                <br>
                <label for="nombreEn">Name</label>
				<input type="text" name="nombreEn" class="form-control" v-model="nombreEn">
				<span v-for="error in errors" class="text-danger">@{{ error.nombreEn }}</span>
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-primary" value="Guardar">
			</div>
		</div>
	</div>
</div>
</form>