<form method="POST" v-on:submit.prevent="updateCountry(fillCountry.identificador)">
<div class="modal fade" id="edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
				<h4>Editar Pais</h4>
			</div>
			<div class="modal-body">
				<label for="nombreEs">Nombre</label>
				<input type="text" name="nombreEs" class="form-control" v-model="fillCountry.nombreEs">
				<span v-for="error in errors" class="text-danger">@{{ error.nombreEs }}</span>
                <br>
                <label for="nombreEn">Name</label>
				<input type="text" name="nombreEn" class="form-control" v-model="fillCountry.nombreEn">
				<span v-for="error in errors" class="text-danger">@{{ error.nombreEn }}</span>
			</div>
			<div class="modal-footer">
				<input type="submit" class="btn btn-primary" value="Actualizar">
			</div>
		</div>
	</div>
</div>
</form>