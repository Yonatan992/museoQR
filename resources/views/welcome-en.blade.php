<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="{{asset('img/fav-icon.png')}}" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>MuseoQR</title>

        <!-- Icon css link -->
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/icofont.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/linear-icon/style.css')}}" rel="stylesheet">
        
        <!-- Bootstrap -->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="{{asset('vendors/revolution/css/settings.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/revolution/css/layers.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/revolution/css/navigation.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/animate-css/animate.css')}}" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="{{asset('vendors/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/owl-carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/swiper/css/swiper.min.css')}}" rel="stylesheet">
        <link href="{{asset('vendors/flipster-slider/jquery.flipster.min.css')}}" rel="stylesheet">
        
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
        <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
         
    </head>
    <body data-spy="scroll" data-target="#bs-example-navbar-collapse-1" data-offset="100">
       
       <div id="preloader">
            <div id="preloader_spinner">
				<div class="pre_inner">
					<div class="dot dot-1"></div>
					<div class="dot dot-2"></div>
					<div class="dot dot-3"></div>
				</div>
            </div>
        </div>
       
        <!--================Header Area =================-->
        <header class="dash_tp_menu_area nine_menu">
            <div class="container">
            	<nav class="navbar navbar-default">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li class="active"><a href="#about">About us</a></li>
							<li><a href="#feature">Characteristics</a></li>
							<li><a href="#team">Museams</a></li>
							<li><a href="#screenshot">Screenshots</a></li>
							<li><a href="#price">Try here</a></li>
                            <li><a href="{{ url('/') }}"><img src="{{asset('img/landing/espana.png')}}"/> Español</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</nav>
            </div>
        </header>
        <!--================End Header Area =================-->
        
        <!--================End Header Area =================-->
        <section class="slider_two_area" id="home">
            <div id="main_slider_ten" class="rev_slider" data-version="5.3.1.6">
                <ul> 
                    <li data-index="rs-2972" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="{{asset('img/landing/home-slider/slider-1.jpg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Web Show" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <!-- LAYERS -->
                        <div class="slider_text_box">
                            <div class="tp-caption first_text" 
                            data-x="['right','right','center','center']" 
                            data-y="['middle','middle','middle','middle']" 
                            data-hoffset="['20','20','0','0']" 
                            data-voffset="['-100','-100','-100','-100']" 
                            data-fontsize="['20','20','20','20','20']" 
                            data-lineheight="['30','30','30','30','30']"
                            data-width="['none','none','none','100%','100%']"
                            data-height="none"
                            data-whitespace="['nowrap','nowrap','nowrap','normal']"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1700,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"x:left(R);","ease":"Power3.easeIn"}]'
                            data-textAlign="['right','right','right','center']">
                            </div>
                            
                            <div class="tp-caption secand_text" 
                            data-x="['right','right','center','center']" 
                            data-y="['middle','middle','middle','middle']" 
                            data-hoffset="['25','36','0','0']" 
                            data-voffset="['-10','0','0','0','-20']"  
                            data-fontsize="['60','40','40','40','30']"
                            data-lineheight="['60','50','50','50','40']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1750,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                            data-textAlign="['right','right','center','center','center']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]">Welcome<br>to the new way <br>to tour a museum</div>
                            
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-2 ten_mobile" 
                                data-x="['-50','-50','-50','-50']" 
                                data-hoffset="['0','0','0','0']" 
                                data-y="['bottom','bottom','bottom','bottom']" 
                                data-voffset="['0','0','0','0']" 
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="image" 
                                data-responsive_offset="on" 
                                data-frames='[{"from":"x:right(R);","speed":800,"to":"o:1;","delay":750,"ease":"Power4.easeOut"},{"delay":"wait","speed":800,"to":"x:left(R);","ease":"Power4.easeIn"}]'
                                data-textAlign="['left','left','left','left']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style="z-index: 5;"><img src="{{asset('img/landing/home-slider/scanner.png')}}" alt="" data-no-retina> 
                            </div>
                        </div>
                    </li>
                    <li data-index="rs-2973" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="{{asset('img/landing/home-slider/slider-1.jpg')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Web Show" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <!-- LAYERS -->
                        <div class="slider_text_box">
                            <div class="tp-caption first_text" 
                            data-x="['right','right','center','center']" 
                            data-y="['middle','middle','middle','middle']" 
                            data-hoffset="['20','20','0','0']" 
                            data-voffset="['-100','-100','-100','-100']" 
                            data-fontsize="['20','20','20','20','20']" 
                            data-lineheight="['30','30','30','30','30']"
                            data-width="['none','none','none','100%','100%']"
                            data-height="none"
                            data-whitespace="['nowrap','nowrap','nowrap','normal']"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1700,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"x:left(R);","ease":"Power3.easeIn"}]'
                            data-textAlign="['right','right','right','center']">
                            </div>
                            
                            <div class="tp-caption secand_text" 
                            data-x="['right','right','center','center']" 
                            data-y="['middle','middle','middle','middle']" 
                            data-hoffset="['25','36','0','0']" 
                            data-voffset="['-10','0','0','0','-20']"  
                            data-fontsize="['60','40','40','40','30']"
                            data-lineheight="['60','50','50','50','40']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-type="text" 
                            data-responsive_offset="on" 
                            data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1750,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                            data-textAlign="['right','right','center','center','center']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]">Be part<br>of a new<br>experience</div>
                            
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-2 ten_mobile" 
                                data-x="['-50','-50','-50','-50']" 
                                data-hoffset="['0','0','0','0']" 
                                data-y="['bottom','bottom','bottom','bottom']" 
                                data-voffset="['0','0','0','0']" 
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="image" 
                                data-responsive_offset="on" 
                                data-frames='[{"from":"x:right(R);","speed":800,"to":"o:1;","delay":750,"ease":"Power4.easeOut"},{"delay":"wait","speed":800,"to":"x:left(R);","ease":"Power4.easeIn"}]'
                                data-textAlign="['left','left','left','left']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style="z-index: 5;"><img src="{{asset('img/landing/home-slider/scanner.png')}}" alt="" data-no-retina> 
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!--================End Header Area =================-->
        
        <!--================Ten Registration Area =================-->
        <section class="ten_registration">
        	<div class="container">
        		
        		<div class="pull-right">
        			
        		</div>
        	</div>
        </section>
        <!--================End Ten Registration Area =================-->
        
        <!--================Powerfull Area =================-->
        <section class="ten_powerfull_area p_100" id="about"> 
        	<div class="container">
        		<div class="ten_title">
        			<h2><span>About us</span></h2>
        			
        		</div>
        		<div class="powerfull_inner">
        			<div class="row">
        				<div class="col-md-3 col-xs-6">
        					<div class="pw_item">
        						<i class="icofont icofont-repair"></i>
        						<h4>Security</h4>
        					</div>
        				</div>
        				<div class="col-md-3 col-xs-6">
        					<div class="pw_item">
        						<i class="icofont icofont-speed-meter"></i>
        						<h4>Nice interface</h4>
        					</div>
        				</div>
        				<div class="col-md-3 col-xs-6">
        					<div class="pw_item">
        						<i class="icofont icofont-light-bulb"></i>
        						<h4>safe functions</h4>
        					</div>
        				</div>
        				<div class="col-md-3 col-xs-6">
        					<div class="pw_item">
        						<i class="icofont icofont-bolt"></i>
        						<h4>trustworthy</h4>
        					</div>
        				</div>
        			</div>
        		</div>
        		<div class="ten_perfomance">
        			<div class="row">
        				<div class="col-md-6">
        					<div class="ten_p_img">
        						<img src="{{asset('img/landing/slide-0.jpg')}}" alt="">
        					</div>
        				</div>
        				<div class="col-md-6">
        					<div class="ten_p_text">
        						
        						<p>
                                    I am Systems graduate in information, enthusiastic with the intention of undertaking, acquiring new skills to create technological solutions that facilitate people's daily lives. The idea came up with the common proposal of a colleague when they observed that when they visited an entity (example: museums) they did not have tools that would allow them to explore the exhibited article in greater detail. The application as a service offers the possibility of accessing a digital collection of articles published by museums that want to make their offer visible. Another service is to be able to scan a QR code that will be associated with an article, allowing access to more information, where you can leave a review followed by a review.
                                </p>
        						
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Powerfull Area =================-->
        
        <!--================Ten Discover Area =================-->
        <section class="ten_discover_area" id="feature">
        	<div class="container">
        		<div class="ten_d_text">
        			<h2>Discover the functions of our application</h2>
        			
        		</div>
        		<div class="ten_discover_inner">
        			<div class="row">
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-1.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Catalogs</h4>
        							<p>Explore the digital collection with more than 2,500 articles published by member museums.</p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-2.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Multimedia</h4>
        							<p>You will find high definition images, texts and article comments.</p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-3.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Listings</h4>
        							<p>Explore the best rated items, categories, and museums. <br><br></p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-4.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Location</h4>
        							<p>Explore the "How to get there" option to see the route from your current location to the chosen entity.</p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-5.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Language</h4>
        							<p>View the content of the application with the language in English and Spanish.</p>
        						</div>
        					</div>
        				</div>
        				<div class="col-md-4">
        					<div class="media">
        						<div class="media-left">
        							<div class="ten_d_icon">
        								<img src="{{asset('img/landing/icon/ten-d-6.png')}}" alt="">
        							</div>
        						</div>
        						<div class="media-body">
        							<h4>Scanner</h4>
        							<p>REGISTER NOW in the APP in a very simple and free way and access the Scanner service and more functions.</p>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Ten Discover Area =================-->
        <!--================Ten Team Area =================-->
        <section class="ten_team_area p_100" id="team">
        	<div class="container">
        		<div class="ten_title">
					<h2><span>Museums that choose us</span></h2>
        		</div>
        		<div class="ten_team_slider owl-carousel">
        			@foreach($entidades as $entidad)
                    <div class="item">
                        <div class="ten_team_item">
                            <img src="{{asset('img/'.$entidad->portada)}}" class="rounded mx-auto d-block" width="200" height="200" alt="">
                            <h4>{{$entidad->nombre}}</h4>
                            <p>Illustrative image</p>
                            <div class="ten_team_hover">
                                <div class="ten_team_h_inner">
                                    <h4>{{$entidad->nombre}}</h4>
                                    <h5>{{$entidad->direccion}}</h5>
                                    <h6>Social networks</h6>
                                    <ul>
                                        <li><a href="{{$entidad->pagina}}"><i class="fa fa-product-hunt"></i></a></li>
                                        <li><a href="{{$entidad->facebook}}"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="{{$entidad->instagram}}"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="{{$entidad->twitter}}"><i class="fa fa-twitter"></i></a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
        	
        		</div>
        	</div>
        </section>
        <!--================End Ten Team Area =================-->
        <!--================Ten App Screen Area =================-->
        <section class="ten_screen_area" id="screenshot">
        	<div class="container">
        		<div class="ten_d_text">
					<h2><span>Screenshots</span> of the app</h2>
        		</div>
        		<div class="flip_ten_slider">
					<ul class="flip-items">
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/principal2.jpeg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/lenguaje2.jpeg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/menu-en.jpg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/menuBarra-en.jpg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/scan-en.jpg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/categoria-en.jpg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/mejVal-en.jpg')}}"  alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/listMuseos-en.jpg')}}" alt="">
						</li>
						<li data-flip-title="Red">
							<img src="{{asset('img/landing/3d-slider/artScaneado1-en.jpg')}}" alt="">
						</li>
                        <li data-flip-title="Red">
                            <img src="{{asset('img/landing/3d-slider/artScaneado2-en.jpg')}}" alt="">
                        </li>
					</ul>
				</div>
        	</div>
        </section>
        <!--================End Ten App Screen Area =================-->       
        
        <!--================Ten Price Area =================-->
        <section class="ten_screen_area" id="price">
        	<div class="container">
                <div class="ten_d_text">
                    <h2><span>Test QR codes</span></h2>
                    <p>Illustrative images</p>
                </div>
        		<div class="row ten_price_list_inner">
        			<div class="col-md-4 col-xs-6">
                        <div class="ten_price_item blue">
                            <div class="ten_price_item_box">
                                <div class="ten_p_head">
                                    <h3>El jaguar, yaguar o yaguareté</h3>
                                    <h5>Museo de Ciencias Naturales <br> "Amado Bonpland"</h5>
                                </div>
                                <ul class="text-center">
                                    <li data-flip-title="Red">
                                        <img src="{{asset('img/landing/codigosQR/codigo1.jpeg')}}" alt="">
                                    </li>
                                </ul>
                            </div>
                            <p class="ten_p_btn"></p>
                        </div>
                    </div>
        			<div class="col-md-4 col-xs-6">
        				<div class="ten_price_item blue">
							<div class="ten_price_item_box">
								<div class="ten_p_head">
									<h3>Barcos medievales</h3>
                                    <h5>Museo Historico de Vias Navegables <br><br></h5>
								</div>
								<ul class="text-center">
									<li data-flip-title="Red">
                                        <img src="{{asset('img/landing/codigosQR/codigo2.jpeg')}}" alt="">
                                    </li>
								</ul>
							</div>
       						<p class="ten_p_btn"></p>
        				</div>
        			</div>
        			<div class="col-md-4 col-xs-6">
                        <div class="ten_price_item blue">
                            <div class="ten_price_item_box">
                                <div class="ten_p_head">
                                    <h3>La noche estrellada</h3>
                                    <h5>Museo Provincial de Bellas Artes <br>"Dr. Juan R. Vidal"</h5>
                                </div>
                                <ul class="text-center">
                                    <li data-flip-title="Red">
                                        <img src="{{asset('img/landing/codigosQR/codigo4.jpeg')}}" alt="">
                                    </li>
                                </ul>
                            </div>
                            <p class="ten_p_btn"></p>
                        </div>
                    </div>
        		</div>
        	</div>
        </section>
        <!--================End Ten Price Area =================-->
        
        <!--================Ten Question Area =================-->
        <section class="ten_question_area">
        	<div class="container">
        		<div class="ten_title">
					<h2>Five questions <span>important</span>about the Museum QR app</h2>
        			
        		</div>
        		<div class="tex_ques_inner">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									How do I get the Museum QR Application?
									<i class="lnr lnr-circle-minus"></i>
									<i class="lnr lnr-plus-circle"></i>
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
								First, open the application store of your cell phone, then select search, then type QR Museum and select from the list. Finally, enter and register in the application.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									How to register in the application?
									<i class="lnr lnr-circle-minus"></i>
									<i class="lnr lnr-plus-circle"></i>
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
								Registering is very simple, complete with your data in the Register section of the application and a message will be sent to your email for verification. Once the email is received, access the link that is attached and that's it.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									How should I use the scanner of the Application?
									<i class="lnr lnr-circle-minus"></i>
									<i class="lnr lnr-plus-circle"></i>
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body">
								To use the scanner you must first of all be registered and verified by the system, if you meet these conditions once you are in the museum in front of the QR code, it is only a matter of going to the Scan section of the application and scan the QR code .
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingfour">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
									How to leave a valuation and a review to a scanned article?
									<i class="lnr lnr-circle-minus"></i>
									<i class="lnr lnr-plus-circle"></i>
									</a>
								</h4>
							</div>
							<div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
								<div class="panel-body">
								Once the QR code has been scanned, it will return all the information of this article, with the possibility of leaving a review followed by a review, it will only have a maximum of 45 minutes to edit it and you can only leave a review of this scanned article.
								</div>
							</div>
						</div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingfive">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                    How to use the English language?
                                    <i class="lnr lnr-circle-minus"></i>
                                    <i class="lnr lnr-plus-circle"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                <div class="panel-body">
                                When starting the application it already gives us the option to choose Spanish and English if we choose the English section, all content will be displayed in that language, when registering, the email that will be sent to you would be in that language, as well as the recovery of password.
                                </div>
                            </div>
                        </div>
					</div>
        		</div>
        	</div>
        </section>
        <!--================End Ten Question Area =================-->
        
        <!--================Footer Area =================-->
        <footer class="ten_footer_area">
            <div class="ten_footer_widget">
            	<div class="container">
            		<div class="row">
            			<div class="col-md-5 col-xs-6">
            				<aside class="t_f_widget t_ab_widget"> 
            					<img src="{{asset('img/ten-f-logo.png')}}" alt="">
            					<h4>We</h4>
            					<p>MuseoQR is a web and mobile system designed for the publication of museum articles in order to motivate visits to entities.</p>
            					
            				</aside>
            			</div>
            			<div class="col-md-3 col-xs-6">
            				<aside class="t_f_widget t_link_widget"> 
            					<div class="ten_f_title">
            						<h3>Refer Link</h3>
            					</div>
            					<ul>
                                    <li><a href="#"><i class="icofont icofont-thin-right"></i> Home</a></li>
                                    <li><a href="#about"><i class="icofont icofont-thin-right"></i> About us</a></li>
                                    <li><a href="#feature"><i class="icofont icofont-thin-right"></i> characteristics</a></li>
                                    <li><a href="#screenshot"><i class="icofont icofont-thin-right"></i> Catches</a></li>
                                    <li><a href="#price"><i class="icofont icofont-thin-right"></i> Try here</a></li>
                                </ul>
                            </aside>
            				</aside>
            			</div>
            			<div class="col-md-4 col-xs-12">
            				<aside class="t_f_widget t_contact_widget"> 
            					<div class="ten_f_title">
            						<h3>Contact with us</h3>
            					</div>
            					<h4>Address</h4>
            					<h5><i class="fa fa-phone"></i> 379- 4895599</h5>
            					<p><i class="fa fa-envelope"></i> infomuseo2020@gmail.com</p>
            					<a class="send_bent" href="#">Send Message</a>
            					
            				</aside>
            			</div>
            		</div>
            	</div>
            </div>
            <div class="ten_f_copyright">
            	<div class="container">
            		<p class="copyright">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a><br>
<a href="#" target="_blank">Terms of Use Privacy Policy</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
            	</div>
            </div>
        </footer>
        <!--================End Footer Area =================-->
        
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{{asset('js/jquery-2.2.4.js')}}"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <!-- Rev slider js -->
        <script src="{{asset('vendors/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
        <script src="{{asset('vendors/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
        <!-- Extra Plugin -->
        <script src="{{asset('vendors/parallax/jquery.parallax-scroll.js')}}"></script>
        <script src="{{asset('vendors/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('vendors/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{asset('vendors/counterup/waypoints.min.js')}}"></script>
        <script src="{{asset('vendors/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('vendors/isotope/imagesloaded.pkgd.min.js')}}"></script>
        <script src="{{asset('vendors/isotope/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('vendors/flexslider/flex-slider.js')}}"></script>
        <script src="{{asset('vendors/flexslider/mixitup.js')}}"></script>
        <script src="{{asset('vendors/nice-selector/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('vendors/swiper/js/swiper.min.js')}}"></script>
        <script src="{{asset('vendors/flipster-slider/jquery.flipster.min.js')}}"></script>
        
        <!--gmaps Js-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
        <script src="{{asset('js/gmaps.min.js')}}"></script>
        
        <script src="{{asset('js/theme.js')}}"></script>
    </body>
</html>