@extends('errors.minimal')

@section('title', __('Falla inesperada. Intente luego'))
@section('code', '500')
@section('message', __('Falla inesperada. Intente luego'))
