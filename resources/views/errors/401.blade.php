@extends('errors.minimal')

@section('title', __('No Autentificado.'))
@section('code', '401')
@section('message', __('No Autentificado.'))
