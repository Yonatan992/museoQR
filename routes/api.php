<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



	//* ******************************************************************************* */
//* ----- Categorias Recursos ---- */
//* ******************************************************************************* */
Route::resource('categories', 'Category\CategoryController', ['except' => ['create','edit']]);
Route::get('categoriesListD', 'Category\CategoryController@deleteList');
Route::get('categoriesRestoreD/{id}', 'Category\CategoryController@restoreDeleted');


//* ----- Filtro por categorias -> Categorias Recursos Anidados ---- */
Route::resource('categories.articles', 'CategoryArticle\CategoryArticleController');



//* ******************************************************************************* */




//* ******************************************************************************* */
/* ----- Paises Recursos ---- */
//* ******************************************************************************* */
Route::resource('countries', 'Country\CountryController', ['except' => ['create','edit']]);
Route::get('countriesListD', 'Country\CountryController@deleteList');
Route::get('countriesRestoreD/{id}', 'Country\CountryController@restoreDeleted');
Route::get('countries-provinces/{id}', 'Country\CountryController@provincias');
Route::get('list-countries', 'Country\CountryController@listaPaises');

//* ******************************************************************************* */





//* ******************************************************************************* */
/* ----- Provincias Recursos ---- */
//* ******************************************************************************* */
Route::resource('provinces', 'Province\ProvinceController', ['except' => ['create','edit']]);
Route::get('provincesListD', 'Province\ProvinceController@deleteList');
Route::get('provincesRestoreD/{id}', 'Province\ProvinceController@restoreDeleted');
Route::get('list-provinces', 'Province\ProvinceController@listaProvincias')->name('provincias');
Route::get('provinces-localities/{id}', 'Province\ProvinceController@localidades')->name('localidades');

//* ******************************************************************************* */




//* ******************************************************************************* */
/* ----- Localidades Recursos ---- */
//* ******************************************************************************* */
Route::resource('localities', 'Locality\LocalityController', ['except' => ['create','edit']]);
Route::get('localitiesListD', 'Locality\LocalityController@deleteList');
Route::get('localitiesRestoreD/{id}', 'Locality\LocalityController@restoreDeleted');


//* ******************************************************************************* */




//* ******************************************************************************* */
/* ----- Entidades Recursos ---- */
//* ******************************************************************************* */
Route::resource('entities', 'Entity\EntityController', ['except' => ['create','edit']]);
Route::get('entitiesListD', 'Entity\EntityController@deleteList');
Route::get('entitiesRestoreD/{id}', 'Entity\EntityController@restoreDeleted');
// Listado de entidades sin Articulos
Route::get('entitiesSinArticles', 'Entity\EntityController@entitiesSinArticles');

Route::get('articulos-entidades/{id}', 'Entity\EntityController@articulosEntidad');


//* ----- Filtro por entidades -> Entidades Recursos Anidados ---- */
Route::resource('entities.articles', 'EntityArticle\EntityArticleController');

//* ----- Filtros ---- */
Route::get('entity/search/{data?}', 'Entity\EntityController@entitySearch');

Route::get('list-entities', 'Entity\EntityController@listEntities');

//* ******************************************************************************* */





//* ******************************************************************************* */
/* ----- Articulos Recursos ---- */
//* ******************************************************************************* */
Route::resource('articles', 'Article\ArticleController', ['except' => ['create','edit']]);

//rutas alternativas articulos elimanados o articulos elimanados por entidad
Route::get('articlesListD/{id?}', 'Article\ArticleController@deleteList');

Route::get('articlesRestoreD/{id}', 'Article\ArticleController@restoreDeleted');

// Listado de Articulos sin Comentarios en general o listado de articulos sin comentarios por entidad
Route::get('articlesSinComments/{id?}', 'Article\ArticleController@articlesSinComments');

//* Listado de articulos y sus comentarios/
Route::get('articlesComments/{slug}', 'Article\ArticleController@comments')->name('comments');


//* ******************************************************************************* */




//* ******************************************************************************* */
/* ----- Comentarios Recursos ---- */
//* ******************************************************************************* */
Route::resource('comments', 'Comment\CommentController', ['except' => ['create','edit']]);
//rutas alternativas comentarios elimanados o comentarios elimanados por articulo
Route::get('commentsListD/{id?}', 'Comment\CommentController@deleteList');
Route::get('commentsRestoreD/{id}', 'Comment\CommentController@restoreDeleted');



/* ******************************************************************************* */





//* ******************************************************************************* */
/* ----- Usuarios Recursos ---- */
//* ******************************************************************************* */
Route::resource('users', 'User\UserController', ['except' => ['create','edit']]);

Route::get('users-entities','User\UserController@userEntidades');
Route::get('users-aplicacion','User\UserController@userAplicacion');
Route::get('users-super','User\UserController@userSuper');
// Listar usuarios eliminados y restaurarlos
Route::get('usersListD/{id?}', 'User\UserController@deleteList');
Route::get('usersRestoreD/{id}', 'User\UserController@restoreDeleted');

/*verificacion de email -- Rutas fluidaa (name)('verify') nos permite intercambiar de una u otra forma los diferentes metodos que interactuan con una ruta especifica*/
Route::name('verify')->get('users/verify/{token}', 'User\UserController@verify');

//Reenviar email para confirmacion de email
Route::name('resend')->get('users/{user}/resend', 'User\UserController@resend');

//Reestablecer contraseña

Route::post('/password/email', 'ApiAuth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset', 'ApiAuth\ResetPasswordController@reset');



//* ******************************************************************************* */





//* ******************************************************************************* */
/* ----- Perfiles Recursos ---- */
//* ******************************************************************************* */
Route::resource('profiles', 'Profile\ProfileController', ['except' => ['create','edit']]);
Route::get('profilesListD', 'Profile\ProfileController@deleteList');
Route::get('profilesRestoreD/{id}', 'Profile\ProfileController@restoreDeleted');



//* ******************************************************************************* */
/* ----- Consultas Avanzadas --- Mejores Valorados ---- */
//* ******************************************************************************* */
//rutas alternativas comentarios elimanados o comentarios elimanados por articulo
Route::get('valorados', 'ConsultaApp\ConsultaAppController@mejoresValorados');

/* ******************************************************************************* */





//* ******************************************************************************* */

//* ----- OAuth2- Passport-> Autentificacion ---- */
Route::post('oauth/token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');


//* ******************************************************************************* */


/*
Users 
*/

Route::post('signup', 'AuthController@signup');
Route::post('login', 'AuthController@login');


Route::get('logout', 'AuthController@logout');
Route::get('user', 'AuthController@user');




//Sistema de logeo ////


Route::group(['prefix' => 'auth'], function () {
	Route::post('signup', 'AuthController@signup');
    Route::post('login', 'AuthController@login');
    
  
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});
