<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('home');
}); 

Route::get('/', 'AdminController@landingEs')->name('landing.español');

Route::get('/en','AdminController@landingEn')->name('landing.ingles');

Route::get('admin', 'AdminController@index')->name('admin');
Route::get('admin/museos', 'AdminController@viewMuseos')->name('museos');
Route::get('admin/paises', 'AdminController@viewPaises')->name('paises');
Route::get('admin/categorias', 'AdminController@viewCategorias')->name('categorias');
Route::get('admin/perfiles', 'AdminController@viewPerfiles')->name('perfiles');
Route::get('admin/entidades', 'AdminController@viewEntidades')->name('entidades');
Route::get('admin/usuarios', 'AdminController@viewUsuarios')->name('usuarios');

Route::get('admin/articulos', 'AdminArticuloController@listArticulos')->name('index.articulos');
Route::delete('delete-articulo/{id}', 'AdminArticuloController@destroy')->name('delete.articulo');
Route::get('create-articulo', 'AdminArticuloController@crearArticulo');
Route::post('store-articulo', 'AdminArticuloController@storeArticulo')->name('store.articulo');
Route::get('edit-articulo/{id}', 'AdminArticuloController@editArticulo');
Route::put('editar-articulo/{id}', 'AdminArticuloController@editarArticulo')->name('edit.articulo');

//rutas alternativas articulos elimanados o articulos elimanados por entidad
Route::get('articulos-eliminados/{id?}', 'AdminArticuloController@listaArtEliminados');
Route::get('articulo-restaurar/{id}', 'AdminArticuloController@restaurarArtEliminados');

// Imprimir codigo QR
Route::name('imprimir')->get('imprimir/{id}', 'AdminArticuloController@imprimirQR');

Route::get('comentarios/{id}', 'AdminArticuloController@comentarios')->name('comentario.show');
Route::delete('comentarios/{id}', 'AdminArticuloController@eliminarComentario')->name('comentario.delete');


//<<<<Reportes
Route::get('reportes-administrador', 'ReportesController@reportes')->name('reportes.admin');

//Cantidad de comentarios por articulos de una determinada entidad
Route::get('count-comment-articles', 'ReportesController@chartCantComent')->name('count.comment');

//cantidad de articulos publicados por las entidades
Route::get('count-articulos-entidades', 'ReportesController@cantidadArticulosEntidad')->name('count.artentidad');

//cantidad de articulos usuarios registrados
Route::get('count-usuarios', 'ReportesController@totalUsuarios')->name('count.usuarios');

//cantidad de usuarios registrados por museos
Route::get('count-users', 'ReportesController@cantidadUserEntidad')->name('count.users');

//Promedio de valoracion por entidad
Route::get('valoracion-articulos-museo', 'ReportesController@mejoresVal')->name('val.articulo');
//<<<<<<<<<<<<


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Password Reset Routes...
if ($options['reset'] ?? true) {
    Route::resetPassword();
}
// Email Verification Routes...
if ($options['verify'] ?? false) {
    $this->emailVerification();
}
Auth::routes();
