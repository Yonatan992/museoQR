<?php
namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser
{

	//Encargado de construir respuestas satifactorias, recibe la informacion a retornar y el codigo de respuestas
	private function successResponse($data, $code)
	{
		//retornara simplemente la informacion con el codigo de respuesta en formato JSON
		return response()->json($data,$code);

	}

	//para los errores. que tendra un mensaje y el codigo
	protected function errorResponse($message, $code)
	{
		return response()->json(['error' => $message, 'code' => $code], $code);
	}

	protected function messagesJson($message, $code)
	{
		return response()->json(['data' => $message, 'code' => $code], $code);
	}

	//dos metodos que muestran una respuesta con multiples elementos (colesccion, por ejemplo una lista de los usuarios) y otro mostrara una instancia especifica
	protected function showAll(Collection $collection, $code = 200)
	{
		if ($collection->isEmpty()){
			return $this->successResponse(['data' => $collection], $code);
		}

		$transformer = $collection->first()->transformer;

		$collection = $this->filterData($collection, $transformer);
		$collection = $this->sortData($collection, $transformer);
		$collection = $this->paginate($collection);
		$collection = $this->transformData($collection,$transformer);
		//$collection = $this->cacheResponse($collection);


		return $this->successResponse($collection, $code);
	}

	protected function showAnterior(Collection $collection, $code = 200)
	{

		return $this->successResponse(['data' => $collection], $code);
	}


	// recibe una instancia de un modelo en especifico
	protected function showOne(Model $instance, $code = 200)
	{
		$transformer = $instance->transformer;
		$instance = $this->transformData($instance,$transformer);

		return $this->successResponse($instance, $code);
	}

	protected function showMessage($message,$code = 200)
	{
		$transformer = $message->first()->transformer;

		$message = $this->transformData($message,$transformer);
		
		return $this->showAnterior($message, $code);
	}

	// Filtrado de datos
	protected function filterData(Collection $collection, $transformer) 
	{
		foreach (request()->query() as $query => $value) {
			$attribute = $transformer::originalAttribute($query);

			if(isset($attribute, $value)){
				$collection = $collection->where($attribute, $value);
			}
		}
		return $collection;
	}

 	// Ordenamiento de datos
	protected function sortData (Collection $collection, $transformer)
	{
		if(request()->has('sort_by')) {
			$attribute = $transformer::originalAttribute(request()->sort_by);

		if (isset($attribute)) {       
			$collection = $collection->sortBy->{$attribute};   
			}
		}

		return $collection;
	}


	protected function paginate (Collection $collection)
	{
		$rules =[
			'per_page' => 'integer|min:2|max:50'

		];

		Validator::validate(request()->all(), $rules);

		$page = LengthAwarePaginator::resolveCurrentPage();

		$perPage = 6;

		if (request()->has('per_page')){
			$perPage = (int) request()->per_page;
		}


		$results = $collection->slice(($page - 1) * $perPage,$perPage)->values();
		$paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page,[
			'path' => LengthAwarePaginator::resolveCurrentPath(),

		]);
	$paginated->appends(request()->all());
	return $paginated;
	}

	protected function transformData($data, $transformer)
	{
		$transformation = fractal($data, new $transformer);
		return $transformation->toArray();

	}

	protected function cacheResponse ($data)
	{
		$url = request()->url();
		$queryParams = request()->query();

		ksort($queryParams);

		//metodo de PHP RECIBIRA EL ARRAY CON CADA UNO DE LOS PARAMETROS DE URL ORDENADOS
		$queryString =http_build_query($queryParams);

		$fullUrl = "{$url}?{$queryString}";

		return Cache::remember($fullUrl,60, function() use($data){
			return $data;
		});
	}

}