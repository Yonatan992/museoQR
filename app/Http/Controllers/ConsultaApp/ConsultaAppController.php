<?php

namespace App\Http\Controllers\ConsultaApp;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class ConsultaAppController extends ApiController
{
   public function __construct()
   {
       $this->middleware('auth:api')->except(['mejoresValorados','verificarUser']);
   }


 
   public function  mejoresValorados (){
   	// $valorados tiene el listado de promedios mayores o iguales de todos los museos sin ordenar
      $valorados = DB::table('articles AS articulos')
            ->leftjoin('comments AS comentarios', 'articulos.id', '=', 'comentarios.article_id')
            ->leftjoin('entities AS entidades', 'articulos.entity_id', '=', 'entidades.id')
            ->select('articulos.titulo as nombre_articulo','articulos.title as name_article','articulos.portada as imagen_articulo',DB::raw('AVG(comentarios.valoracion) as promedio_articulo'),'entidades.nombre as nombre_museo')
            ->where('articulos.deleted_at','=',NULL)
            ->groupBy('articulos.titulo','entidades.nombre')
            ->having('promedio_articulo', '>=', 4)
            //->orderByRaw([['entidades.nombre','asc'],['promedio_articulo', 'desc']])
            ->get();

      // $resourse => creamos un array (con lo que hay en $valorados->collection)
      $resources=[];
      foreach ($valorados as $value) {
		$resources[]=[
			'nombre_museo'=> $value->nombre_museo,
			'nombre_articulo'=> $value->nombre_articulo,
         'name_article'=> $value->name_article,
			'imagen_articulo'=> $value->imagen_articulo,
			'promedio_articulo'=> $value->promedio_articulo
		];
      	# code...
      }
      // creamos dos arrays para obtener los nombre de museos y promedio para luego pasarlos a array_multisort
      $museo=[];
      $promedio=[];
      foreach ($valorados as $key => $row)
	   {
		    $museo[$key] = $row->nombre_museo;
		    $promedio[$key] = $row->promedio_articulo;
		}
		// array_multisort ordenamos el array con los parametros $museo y $promedio (devuelve true o false si ordena o no) al pasar el array $resources ordenamos y mostramos el array $resources
      array_multisort($museo, SORT_ASC, $promedio, SORT_DESC, $resources);
      
      // volvemos a convertir en collection para poder mostrar como Json que es lo que nos pide la funcion showAnterior
      return $this->showAnterior(collect($resources));
       
   }

   
}

