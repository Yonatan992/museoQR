<?php

namespace App\Http\Controllers\Entity;

use App\Models\Entity;
use Illuminate\Http\store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Transformers\EntityTransformer;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\EntityStoreRequest;
use App\Http\Requests\EntityUpdateRequest;
use Image;

class EntityController extends ApiController
{
     public function __construct()
    {
        //$this->middleware('auth:api')->except(['destroy','restoreDeleted','deleteList','store','index','show','entitySearch','update']);
        
        $this->middleware('transform.input:'. EntityTransformer::class)->only(['store','update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entity = Entity::orderBy('id','DESC')->get();
        return $this->showAll($entity);
    }

    public function listEntities(){
        $entidades = Entity::orderBy('id', 'DESC')->get();
        return $this->showAnterior($entidades);

    }


     public function articulosEntidad($entity){

        $entidad = Entity::findOrFail($entity);
        $articles = $entidad->articles()->get();
        return $this->showAll($articles);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntityStoreRequest $request)
    {
     
        $data = $request->all();
        if ($request->hasFile('portada')){
            $image_resize = Image::make($request->portada->getRealPath());
            $image_resize->resize(800, 800, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
            });
            $image_resize->orientate();
            $minuscula = strtolower($request->nombre);
            $nombre_minuscula = str_replace(" ", "-", $minuscula);
            $nombre_archivo = $nombre_minuscula . "." . $request->portada->extension();
            $image_resize->save(public_path('img/imgMuseo/' . $nombre_archivo,50));
            $data['portada'] = 'imgMuseo/'.$nombre_archivo;
        }
        $entity= Entity::create($data);


        return $this->showOne($entity, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Entity $entity)
    {
        $entidad = Entity::findOrFail($entity->id);
        return $this->showOne($entidad, 200);
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EntityUpdateRequest $request, Entity $entity)
    {
        $data =$request->all();

        if ($request->hasFile('portada')){
            Storage::delete($entity->portada);

            $image_resize = Image::make($request->portada->getRealPath());
            $image_resize->resize(800, 800, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
            });
            $image_resize->orientate();
            $minuscula = strtolower($request->nombre);
            $nombre_minuscula = str_replace(" ", "-", $minuscula);
            $nombre_archivo = $nombre_minuscula . "." . $request->portada->extension();
            $image_resize->save(public_path('img/imgMuseo/'.$nombre_archivo,50));
            $url = 'imgMuseo/'.$nombre_archivo;
            $data['portada'] = $url;
        }

        $entity->fill($data)->save();

 
        return $this->showOne($entity,200);
        



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entity $entity)
    {
            //Storage::delete($entity->portada); //removemos la imagem del Entidad
            //$entity->article()->delete();
            $entidad = Entity::findOrFail($entity->id);
            Entity::deleted(function($entidad){
                    $entidad->articles()->delete();
                    $entidad->users()->delete();
                });
            $entidad->delete();
            return $this->showOne($entidad);
    }


    public function deleteList()
    {
        //
        $entity = Entity::onlyTrashed()->get();
       return $this->showAll($entity);
    }


    public function restoreDeleted($id)
    {

        Entity::withTrashed()->where('id', $id)->restore();
        $entity = Entity::findOrFail($id); 
        $entity->articles()->withTrashed()->restore();
        $entity->users()->withTrashed()->restore();
    

        return $this->showOne($entity);
    }



    public function entitiesSinArticles()
    {
        $entities = Entity::doesntHave('articles')->get();
        return $this->showAll($entities);

    }

    public function entitySearch($data = null)
    {

        $entity = Entity::where( function ($query) use ($data){
             $data === null
                 ? $data
                 : $query->where('nombre', 'LIKE', '%'. $data. '%');
                })->get();
                //$entity= Entity::where('nombre', 'LIKE', '%'. $data. '%' )->get();

    return $this->showAll($entity);

    }


}
