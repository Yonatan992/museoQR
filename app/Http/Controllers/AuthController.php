<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Passport\Passport;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use App\Http\Requests\SignupCreateRequest;

class AuthController extends ApiController
{

   public function __construct()
    {
        $this->middleware('auth:api')->except(['login','signup','logout']);
        
    }

   public function signup(SignupCreateRequest $request)
   {
     
      $user = new User([
        'profile_id'          => $request->profile_id,
        'entity_id'           => $request->entity_id, 
        'lang'                => $request->lang, 
        'firstname'           => $request->firstname,
        'lastname'            => $request->lastname,
        'email'               => $request->email,
        'password'            => bcrypt($request->password),
        'verified'            => User::USUARIO_NO_VERIFICADO,
        'verification_token'  => User::generarVerificationToken(),
    ]);
      $user->save();

      $tokenResult = $user->createToken('Personal Access Token');
      $token = $tokenResult->token;
      $token->expires_at = Carbon::now()->addWeeks(12);
      $token->save();

      return response()->json([
        'user'=>$user, 
        'access_token'=>$tokenResult->accessToken,
        'token_type'   => 'Bearer',
        'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString()],200);
  }



  public function login(LoginRequest $request)
  {
     
     $credentials = $request->all();
     if (!auth::attempt($credentials)) {
        return response()->json([
            'message' => 'Credenciales Invalidas'],401);
    }
    
    $tokenResult = auth()->user()->createToken('Personal Access Token');
    $token = $tokenResult->token;
    $token->expires_at = Carbon::now()->addWeeks(12);
    $token->save();

    return response()->json([
      'user'=>auth()->user(),
      'access_token' => $tokenResult->accessToken,
      'token_type'   => 'Bearer',
      'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString()],200);

}




public function logout(Request $request)
{
    $request->user()->token()->revoke();
    return response()->json(['message' => 
        'Ha Cerrado Sesion exitosamente']);
}




public function user(Request $request)
{
    return response()->json($request->user());
}




}
