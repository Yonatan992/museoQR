<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use App\Models\Article;
use Illuminate\Http\Request;
use ConsoleTVs\Charts\Facades\Charts;


class ReportesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function reportes()
    {
        return view('reportes.comen_art');
    }

    public function chartCantComent()
    {
       	$query = DB::table('articles AS articulos')
            ->leftjoin('comments AS comentarios', 'articulos.id', '=', 'comentarios.article_id')
            ->select('articulos.titulo as nombre_articulo','articulos.title as name_article',DB::raw('COUNT(comentarios.id) as count_commet'))
            ->where('articulos.deleted_at','=',NULL)
            ->where('comentarios.deleted_at','=',NULL)
            ->where('articulos.entity_id', auth()->user()->entity_id)
            ->groupBy('articulos.id')
            ->get();

        if (count($query) == 0) {
            return view('reportes.comen_art');

        }

        foreach ($query as $consulta) {
            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
            $titulos[] = $consulta->nombre_articulo;
            $cantidad[] =$consulta->count_commet;
        }


        $comentarios = Charts::create('pie', 'highcharts')
                ->title('Cantidad de comentarios por articulo')
                ->labels($titulos)
                ->values($cantidad)
                ->colors($colours)
                ->dimensions(1000,500)
                ->responsive(true);

        return view('reportes.comen_art',compact('comentarios'));

    }


    public function cantidadArticulosEntidad()
    {
        $query = DB::table('entities AS ent')
            ->leftjoin('articles AS art', 'ent.id', '=', 'art.entity_id')
            ->select('ent.nombre as nombre_entidad',DB::raw('COUNT(art.id) as count_art'))
            ->where('ent.deleted_at','=',NULL)
            ->where('art.deleted_at','=',NULL)
            ->groupBy('ent.id')
            ->get();

         if (count($query) == 0) {
            return view('reportes.cant_art_entidades');

        }

        foreach ($query as $consulta) {
            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
            $nombre[] = str_replace('"','',$consulta->nombre_entidad);
            $cantidad[] =$consulta->count_art;
        }

        $ent = Charts::create('bar', 'highcharts')
                ->title('Cantidad de artículos por entidad')
                ->labels($nombre)
                ->elementLabel("Cantidad de artículos publicados")
                ->values($cantidad)
                ->colors($colours)
                ->dimensions(1000,600)
                ->responsive(true);

        return view('reportes.cant_art_entidades',compact('ent'));

    }


    public function totalUsuarios()
    {
        $users = User::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
                    ->get();

        if (count($users) == 0) {
            return view('reportes.cant_usuarios');

        }

        $usuarios = Charts::database($users, 'bar', 'highcharts')
                  ->title("Nuevos usuarios registrados mensualmente")
                  ->elementLabel("Total Usuarios")
                  ->dimensions(1000, 500)
                  ->responsive(true)
                  ->groupByMonth(date('Y'), true);


        return view('reportes.cant_usuarios',compact('usuarios'));

    }


     public function cantidadUserEntidad()
    {
        $query = DB::table('users AS user')
            ->leftjoin('entities AS ent', 'ent.id', '=', 'user.entity_id')
            ->select('ent.nombre as nombre_entidad',DB::raw('COUNT(user.id) as count_user'))
            ->where('ent.deleted_at','=',NULL)
            ->where('user.deleted_at','=',NULL)
            ->where('user.entity_id','!=',NULL)
            ->groupBy('ent.id')
            ->get();

         if (count($query) == 0) {
            return view('reportes.cant_user_entidades');

        }

        foreach ($query as $consulta) {
            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
            $nombre[] = str_replace('"','',$consulta->nombre_entidad);
            $cantidad[] =$consulta->count_user;
        }

        $users = Charts::create('bar', 'highcharts')
                ->title('Cantidad de usuarios por Museo')
                ->labels($nombre)
                ->elementLabel("Cantidad de usuarios")
                ->values($cantidad)
                ->colors($colours)
                ->dimensions(1000,600)
                ->responsive(true);

        return view('reportes.cant_user_entidades',compact('users'));

    }


    public function mejoresVal()
    {
         $query = DB::table('articles AS articulos')
            ->leftjoin('comments AS comentarios', 'articulos.id', '=', 'comentarios.article_id')
            ->leftjoin('entities AS entidades', 'articulos.entity_id', '=', 'entidades.id')
            ->select('articulos.titulo as nombre_articulo',DB::raw('AVG(comentarios.valoracion) as promedio_articulo'))
            ->where('articulos.deleted_at','=',NULL)
            ->where('articulos.entity_id', auth()->user()->entity_id)
            ->groupBy('articulos.titulo')
            //->orderByRaw([['entidades.nombre','asc'],['promedio_articulo', 'desc']])
            ->get();

        if (count($query) == 0) {
            return view('reportes.val_art_entidades');

        }

        foreach ($query as $consulta) {
            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
            $nombre_art[] = $consulta->nombre_articulo;
            $prom_val[] = round($consulta->promedio_articulo, 2);
        }

        $valoraciones = Charts::create('bar', 'highcharts')
                ->title('Valoración de artículos')
                ->labels($nombre_art)
                ->elementLabel("valoración promedio")
                ->values($prom_val)
                ->colors($colours)
                ->dimensions(1000,600)
                ->responsive(true);

        return view('reportes.val_art_entidades',compact('valoraciones'));

    }



}
