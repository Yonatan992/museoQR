<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Entity;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['landingEs','landingEn']);
    }

    public function index()
    {
        if (auth()->user()->esVisitante()){
            //Desconctamos al usuario
            Auth::logout();

            $entidades = Entity::orderBy('id','DESC')->get();
            return view('welcome')->with(['entidades' => $entidades]);        }
        return view('admin');
    }

    public function viewMuseos()
    {
        return view('admin.museos.index');
    }

    public function viewPaises()
    {
        
        return view('admin.paises.index');
    }

    public function viewCategorias()
    {
        
        return view('admin.categorias.index');
    }

    public function viewPerfiles()
    {
        
        return view('admin.perfiles.index');
    }

    public function viewEntidades()
    {
        
        return view('admin.entidades.index');
    }

    public function viewUsuarios()
    {
        
        return view('admin.usuarios.index');
    }

     public function viewArticulos()
    {
        
        return view('admin.articulos.index');
    }

    public function landingEs()
    {
        $entidades = Entity::orderBy('id','DESC')->get();
        return view('welcome')->with(['entidades' => $entidades]);
    }

     public function landingEn()
    {
        $entidades = Entity::orderBy('id','DESC')->get();
        return view('welcome-en')->with(['entidades' => $entidades]);
    }


}
