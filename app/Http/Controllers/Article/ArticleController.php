<?php

namespace App\Http\Controllers\Article;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use App\Transformers\ArticleTransformer;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\ArticleUpdateRequest;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;

class ArticleController extends ApiController
{
    public function __construct()
    {
        //$this->middleware('auth:api')->except(['index','show','deleteList','articlesSinComments']);

        $this->middleware('transform.input:'. ArticleTransformer::class)->only(['store','update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = Article::orderBy('id','DESC')->get();

        return $this->showAll($article,200);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleStoreRequest $request)
    {
       $data = $request->all();
       $data['portada'] = $request->portada->store('imgArticles');

       $article= Article::create($data);
       $idurl = $article['slug'];
                $image = \QrCode::format('png')
                 //->merge('img/t.jpg', 0.1, true)
                        ->size(200)->errorCorrection('H')
                        ->generate($idurl);
                $output_file = '/CodesQR/img-' .$idurl.'-'.$request->titulo . '.png';
            Storage::disk('')->put($output_file, $image);
            $url ='CodesQR/img-' .$idurl.'-'.$request->titulo . '.png';

            $data['codeQr'] = $url;
            $article->fill($data)->save();

        return $this->showOne($article, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)

    {
        $articleOne= Article::where('slug', $slug)->with('category','entity','comments.user')->get();
        return $this->showAll($articleOne, 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleUpdateRequest $request, Article $article)
    {

        $data =$request->all();
        Storage::delete($article->portada);
        $data['portada'] = $request->portada->store('');

        $article->fill($data)->save();
        return $this->showOne($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        Storage::delete($article->portada); //removemos la imagem del articulo

        $article->delete();
        return $this->showOne($article);
    }

        //  $articles = $id->articles()::onlyTrashed()
        // ->with('category','entity')->get();
        // return $this->showAll($articles);

// rutas alternativa si no le paso un valor me muestra todos los eliminados, si le paso un id entidad me muestra los articulos elimandos de esa entidad
     public function deleteList($id = null)
    {

        $article = Article::onlyTrashed()->where(function ($query) use($id) {
            $id === null
                ? $id
                : $query->where('entity_id', $id);
        })->get();
       return $this->showAll($article);
  

    }



    public function restoreDeleted($id)
    {
        
        Article::withTrashed()->where('id', $id)->restore();
        $article = Article::findOrFail($id); 
        return $this->showOne($article);
    }

    public function articlesSinComments($id = null)
    {
         $articles = Article::doesntHave('comments')->where( function ($query) use ($id){
             $id === null
                 ? $id
                 : $query->where('entity_id', $id);
                })->get();

        // $articles = Article::doesntHave('comments')->get();
        return $this->showAll($articles);

    }

    public function comments ($slug)
    {
         $article= Article::where('slug', $slug)->firstOrFail();
         $comment = $article->comments;
        return $this->showAll($comment);
    }


    
}
