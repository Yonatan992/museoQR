<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Mail\UserCreated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Transformers\UserTransformer;
use App\Http\Controllers\ApiController;
use App\Http\Requests\SignupUpdateRequest;

class UserController extends ApiController
{
     public function __construct()
    {
        //parent::__construct();
        
        $this->middleware('transform.input:'. UserTransformer::class)->only(['store']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id','DESC')->get();
        return $this->showAll($users);
    }


    public function userEntidades (){
        $usuarios = User::orderBy('id', 'DESC')
        ->where('profile_id',2)->get();

        return $this->showAnterior($usuarios);

    }

     public function userAplicacion(){
        $usuarios_app = User::orderBy('id', 'DESC')
        ->where('profile_id',3)->get();

        return $this->showAll($usuarios_app);

    }

    public function userSuper(){
        $usuarios_sup = User::orderBy('id', 'DESC')
        ->where('profile_id',1)->get();

        return $this->showAll($usuarios_sup);

    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::findOrFail($id);

        return $this->showOne($usuario, 200);
    }
   


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SignupUpdateRequest $request, $id)
    {
        $usuario = User::findOrFail($id);

        if ($request->has('profile_id') && $usuario->profile_id != $request->profile_id)
        {
            $usuario->profile_id = $request->profile_id;
        }

        if ($request->has('entity_id') && $usuario->entity_id != $request->entity_id)
        {
            $usuario->entity_id = $request->entity_id;
        }

        if ($request->has('lang') && $usuario->lang != $request->lang)
        {
            $usuario->lang = $request->lang;
        }

        if ($request->has('firstname') && $usuario->firstname != $request->firstname)
        {
            $usuario->firstname = $request->firstname;
        }

        if ($request->has('lastname') && $usuario->lastname != $request->lastname)
        {
            $usuario->lastname = $request->lastname;
        }

        if ($request->has('email') && $usuario->email != $request->email)
        {
            $usuario->email = $request->email;
        }

        $usuario->save();

        return $this->showOne($usuario,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::findOrFail($id);
        $usuario->delete();
        return $this->showOne($usuario, 200);
    }

    public function verify ($token)
    {
        $user = User::where('verification_token', $token)->firstOrFail();
        $user->verified = User::USUARIO_VERIFICADO;
        $user->verification_token = null;
        $user->save();

        //return $this->messagesJson('La cuenta ha sido verificada',200);
        if($user->lang == 'es'){
            return view('mensajes.verify-es');
        }elseif($user->lang == 'en') {
            return view('mensajes.verify-en');
        }
    }

    public function resend (User $user)
    {

        if ($user->esVerificado()) {
            return $this->errorResponse('Este usuario ya ha sido verificado', 409);
        }

         retry(5, function() use($user) {
                Mail::to($user)->send(new UserCreated($user));
            }, 100);
        return $this->messagesJson('el correo de verificacion se ha reenviado',200);
    }


    public function deleteList()
    {
        //
        $usuarios = User::onlyTrashed()->get();
        return $this->showAll($usuarios);
    }



    public function restoreDeleted($id)
    {
        
        User::withTrashed()->where('id', $id)->restore();
        $usuario = User::findOrFail($id); 
        return $this->showOne($usuario);
    }



}
