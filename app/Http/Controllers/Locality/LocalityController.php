<?php

namespace App\Http\Controllers\Locality;

use App\Models\Locality;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Transformers\LocalityTransformer;
use App\Http\Requests\LocalityStoreRequest;
use App\Http\Requests\LocalityUpdateRequest;

class LocalityController extends ApiController
{
     public function __construct()
    {
        //parent::__construct();
        
        $this->middleware('transform.input:'. LocalityTransformer::class)->only(['store','update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $localidades = Locality::with('province.country:id,nombre')->get();
        return $this->showAll($localidades);
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocalityStoreRequest $request)
    {
        

       $locality= Locality::create($request->all());

        return $this->showOne($locality, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Locality $locality)
    {
        $locality= Locality::with('province.country:id,nombre')->findOrFail($locality->id);
        return $this->showOne($locality, 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LocalityUpdateRequest $request, Locality $locality)
    {
  
        $locality->fill($request->all())->save();
        return $this->showOne($locality);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Locality $locality)
    {
        $locality->delete();
        return $this->showOne($locality);
    }

    public function deleteList()
    {
        //
        $locality = Locality::onlyTrashed()->get();
       return $this->showAll($locality);
    }

    public function restoreDeleted($id)
    {
        
        Locality::withTrashed()->where('id', $id)->restore();
        $locality = Locality::findOrFail($id); 
        return $this->showOne($locality);
    }
}
