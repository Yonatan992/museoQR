<?php

namespace App\Http\Controllers\ApiAuth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

 

    protected function sendResetLinkResponse($response)
    {
        return response()->json(['message' => 'Correo electrónico de recuperación enviado.'],200);
    }


    protected function sendResetLinkFailedResponse($response)
    {
        return response()->json(['error' => 'Algo salió mal, Verifique el email.'],422);
    
    }
}
