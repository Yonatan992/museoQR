<?php

namespace App\Http\Controllers\Profile;

use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Transformers\ProfileTransformer;
use App\Http\Requests\ProfileStoreRequest;
use App\Http\Requests\ProfileUpdateRequest;

class ProfileController extends ApiController
{
     public function __construct()
    {
        //parent::__construct();
        
        $this->middleware('transform.input:'. ProfileTransformer::class)->only(['store','update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $profiles = Profile::all();

        return $this->showAll($profiles);
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileStoreRequest $request)
    {
        $profile= Profile::create($request->all());

        return $this->showOne($profile, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        return $this->showOne($profile, 200);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileUpdateRequest $request, Profile $profile)
    {
         $profile->fill($request->all())->save();

        return $this->showOne($profile);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $profile->delete();
        return $this->showOne($profile);
    }


    public function deleteList()

    {
        //
        $profiles = Profile::onlyTrashed()->get();
       return $this->showAll($profiles);
    }


    public function restoreDeleted($id)
    {
        
        Profile::withTrashed()->where('id', $id)->restore();
        $profile = Profile::findOrFail($id); 
        return $this->showOne($profile);
    }



}
