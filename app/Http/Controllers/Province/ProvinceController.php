<?php

namespace App\Http\Controllers\Province;

use App\Models\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Transformers\ProvinceTransformer;
use App\Http\Requests\ProvinceStoreRequest;
use App\Http\Requests\ProvinceUpdateRequest;

class ProvinceController extends ApiController
{


    public function __construct()
    {
        
        
        $this->middleware('transform.input:'.ProvinceTransformer::class)->only(['store','update']);


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $province = Province::with('country:id,nombre')->get();
        return $this->showAll($province);
    }

    public function listaProvincias()
    {
        $provinces = Province::orderBy('id','DESC')->get();
        return $this->showAnterior($provinces);
    }

    public function localidades($id)
    {
        $province = Province::findOrFail($id);
        $localidades = $province->localities()->get();
        return $this->showAnterior($localidades);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProvinceStoreRequest $request)
    {


       $province= Province::create($request->all());

        return $this->showOne($province, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Province $province)
    {
        $province = Province::with('country:id,nombre')->findOrFail($province->id);
        return $this->showOne($province, 200);
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProvinceUpdateRequest $request, Province $province)
    {


        $province->fill($request->all())->save();

        return $this->showOne($province);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Province $province)
    {
        $province->delete();
        return $this->showOne($province);
    }

    public function deleteList()
    {
        //
        $province = Province::onlyTrashed()->get();
       return $this->showAll($province);
    }


    public function restoreDeleted($id)
    {
        
        Province::withTrashed()->where('id', $id)->restore();
        $province = Province::findOrFail($id); 
        return $this->showOne($province);
    }

    
}
