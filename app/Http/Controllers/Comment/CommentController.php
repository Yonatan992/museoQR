<?php

namespace App\Http\Controllers\Comment;

use App\User;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Transformers\CommentTransformer;
use App\Http\Requests\CommentStoreRequest;
use App\Http\Requests\CommentUpdateRequest;


class CommentController extends ApiController
{
     public function __construct()
    {
        //parent::__construct();
        
        $this->middleware('transform.input:'. CommentTransformer::class)->only(['store','update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commentary = Comment::with('article','user')->get();

        return $this->showAll($commentary);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentStoreRequest $request)
    {
        $commentary= Comment::create($request->all());

        return $this->showOne($commentary, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        $commentOne= Comment::where('id', $comment->id)->with('article','user')->get();
        return $this->showAll($commentOne, 200);
    }


        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentUpdateRequest $request,Comment $comment)
    {
        $comment->fill($request->all())->save();

        return $this->showOne($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return $this->showOne($comment,200);
    }


    public function deleteList($id = null)
    {
        
         $comment= Comment::onlyTrashed()->where(function ($query) use($id) {
            $id === null
                ? $id
                : $query->where('article_id', $id);
        })->get();
       return $this->showAll($comment);


       //  $comment = Comment::onlyTrashed()->get();
       // return $this->showAll($comment);
    }

    public function restoreDeleted($id)
    {
        
        Comment::withTrashed()->where('id', $id)->restore();
        $comment = Comment::findOrFail($id); 
        return $this->showOne($comment);
    }
}
