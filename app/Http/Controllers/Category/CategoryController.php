<?php

namespace App\Http\Controllers\Category;

use App\Models\Category;
use App\Rules\alphaSpaces;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use App\Transformers\CategoryTransformer;
use Illuminate\Database\Eloquent\restore;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use Image;

class CategoryController extends ApiController
{
     public function __construct()
    {
        
        $this->middleware('auth:api')->except(['index','update','store','show','destroy','deleteList','restoreDeleted']);
        
        $this->middleware('transform.input:'. CategoryTransformer::class)->only(['store','update']);


    }


    /**
        * @OA\Info(title="API Categories", version="1.0")
        *
        * @OA\Server(url="http://localhost:8000")
*/
    public function index()
    {

        /**
    * @OA\Get(
    *     path="/api/categories",
    *     summary="Mostrar todas las Categorias disponible ",
    *     @OA\Response(
    *         response=200,
    *         description="Muestra todas las categorias disponibles."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */

        $category = Category::orderBy('id','DESC')->get();

        return $this->showAll($category,200);
    }



    public function store(CategoryStoreRequest $request)
    {
        $data = $request->all();

        $data['image'] = $request->file('image')->store('img/imgCategorias');
        
        $category= Category::create($data);

        return $this->showOne($category, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $category = Category::findOrFail($category->id);

        return $this->showOne($category, 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        $categoria = Category::findOrFail($category->id);

        if ($request->has('nombre') && $categoria->nombre != $request->nombre)
        {
            $categoria->nombre = $request->nombre;
        }

        if ($request->has('name') && $categoria->name != $request->name)
        {
            $categoria->name = $request->name;
        }

        if (!empty($request->image) && !empty($categoria->image)){
            Storage::delete($categoria->image);
            $categoria->image = $request->image->store('img/imgCategorias');
        }

        if (!empty($request->image) && empty($categoria->image)){
            $categoria->image = $request->image->store('img/imgCategorias');
        }
        

        $categoria->save();

        return $this->showOne($categoria,200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Category $category)
    {
        //$category = Category::findOrFail($id);
        $category->delete();
        return $this->showOne($category);
    }


    public function deleteList()
    {
        //
        $category = Category::onlyTrashed()->get();
       return $this->showAll($category);
    }
    

     public function restoreDeleted($id)
    {
        
        Category::withTrashed()->where('id', $id)->restore();
        $category = Category::findOrFail($id); 
        return $this->showOne($category);
    }

}
