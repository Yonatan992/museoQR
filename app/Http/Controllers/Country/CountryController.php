<?php

namespace App\Http\Controllers\Country;

use App\Models\Country;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Transformers\CountryTransformer;
use App\Http\Requests\CountryStoreRequest;
use App\Http\Requests\CountryUpdateRequest;

class CountryController extends ApiController
{
     public function __construct()
    {
        //parent::__construct();
        
        $this->middleware('transform.input:'. CountryTransformer::class)->only(['store','update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = Country::orderBy('id','DESC')->get();
        return $this->showAll($country);
    }


    public function listaPaises()
    {
        $country = Country::orderBy('id','DESC')->get();
        return $this->showAnterior($country);
    }


    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryStoreRequest $request)
    {
      
        
       $country= Country::create($request->all());

        return $this->showOne($country, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //$country = Country::findOrFail($id);

        return $this->showOne($country, 200);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CountryUpdateRequest $request, Country $country)
    {
        //$country = Country::findOrFail($id);


       $country->fill($request->all())->save();

        return $this->showOne($country);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //$country = Country::findOrFail($id);
        $country->delete();
        return $this->showOne($country);
    }


    public function deleteList()
    {
        //
        $country = Country::onlyTrashed()->get();
       return $this->showAll($country);
    }



    public function restoreDeleted($id)
    {
        
        Country::withTrashed()->where('id', $id)->restore();
        $country = Country::findOrFail($id); 
        return $this->showOne($country);
    }

    public function provincias($id)
    {
        $country = Country::findOrFail($id);
        $provincias = $country->provinces()->get();
        return $this->showAnterior($provincias);
    }
}
