<?php

namespace App\Http\Controllers;

use PDF;
use Image;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\ArticleUpdateRequest;


class AdminArticuloController extends Controller
{
	public function __construct(){

		$this->middleware('auth');
	}
            
        
        
    public function listArticulos() {
		$articulos = Article::orderBy('id', 'DESC')
			->where('entity_id', auth()->user()->entity_id)
			->paginate(5);

		return view('admin.articulos.panel', compact('articulos'));

    }

    public function destroy($id)
    {
        $articulo = Article::findOrFail($id);
        $articulo1 = Article::findOrFail($id);
        //$this->authorize('passArticle', $articulo); // si es mio elimina el articulo (Control de Acceso)
        $articulo1->delete();
        return redirect()->route('index.articulos')->with('info','El artículo '.$articulo->titulo.' fue Eliminado !');
    }

    public function listaArtEliminados($id = null)
    {
        $articulos = Article::onlyTrashed()->where(function ($query) use($id) {
            $id === null
                ? $id
                : $query->where('entity_id', $id);
        })->paginate(5);
       
        return view('admin.articulos.eliminados')->with(['articulos' => $articulos]);  

    }

    public function restaurarArtEliminados($id)
    {
        
        Article::withTrashed()->where('id', $id)->restore();
        $articulo = Article::findOrFail($id); 
         return redirect()->back()->with('info','El artículo '.$articulo->titulo.' fue Restaurado !');
    }

    public function crearArticulo()
    {
        $categorias= Category::all();
        return view('admin.articulos.create',compact('categorias'));
    }

    public function storeArticulo(ArticleStoreRequest $request)
    {
        $data = $request->all();
        //$data['portada'] = $request->portada->store('imgArticles');

        if ($request->hasFile('portada')){
            $image_resize = Image::make($request->portada->getRealPath());
            $image_resize->resize(800, 800, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
            });
            $image_resize->orientate();
            $minuscula = strtolower($request->titulo."-".$request->title);
            $nombre_minuscula = str_replace(" ", "-", $minuscula);
            $nombre_archivo = $nombre_minuscula . "." . $request->portada->extension();
            $image_resize->save(public_path('img/imgArticles/' . $nombre_archivo,50));
            $data['portada'] = 'imgArticles/'.$nombre_archivo;
        }


       $article= Article::create($data);
       $idurl = $article['slug'];
                $image = \QrCode::format('png')
                 //->merge('img/t.jpg', 0.1, true)
                        ->size(200)->errorCorrection('H')
                        ->generate($idurl);
                $output_file = '/CodesQR/img-' .$idurl.'-'.$request->titulo . '.png';
            Storage::disk('')->put($output_file, $image);
            $url ='CodesQR/img-' .$idurl.'-'.$request->titulo . '.png';

            $data['codeQr'] = $url;
            $article->fill($data)->save();

        return back()->with('message', 'Su articulo fue Publicado !');
    }

    public function editArticulo($id)
    {
        $articulo = Article::findOrFail($id);
        $categorias= Category::all();
        return view('admin.articulos.edit',compact('articulo','categorias'));
    }

    public function editarArticulo(ArticleUpdateRequest $request, $id)
    {
        $articulo = Article::findOrFail($id);

        if ($request->has('category_id') && $articulo->category_id != $request->category_id)
        {
            $articulo->category_id = $request->category_id;
        }

        if ($request->has('status') && $articulo->status != $request->status)
        {
            $articulo->status = $request->status;
        }

        if ($request->has('titulo') && $articulo->titulo != $request->titulo)
        {
            $articulo->titulo = $request->titulo;
        }

        if ($request->has('title') && $articulo->title != $request->title)
        {
            $articulo->title = $request->title;
        }

        if ($request->has('subtitulo') && $articulo->subtitulo != $request->subtitulo)
        {
            $articulo->subtitulo = $request->subtitulo;
        }

        if ($request->has('subtitle') && $articulo->subtitle != $request->subtitle)
        {
            $articulo->subtitle = $request->subtitle;
        }

        if ($request->has('descripcion') && $articulo->descripcion != $request->descripcion)
        {
            $articulo->descripcion = $request->descripcion;
        }
        if ($request->has('description') && $articulo->description != $request->description)
        {
            $articulo->description = $request->description;
        }

        if ($request->has('cuerpo') && $articulo->cuerpo != $request->cuerpo)
        {
            $articulo->cuerpo = $request->cuerpo;
        }
        if ($request->has('body') && $articulo->body != $request->body)
        {
            $articulo->body = $request->body;
        }

        if ($request->hasFile('portada')){
            $mi_imagen = public_path().$articulo->portada;
                if (@getimagesize($mi_imagen)) {
                    unlink($mi_imagen);
                }

            $image_resize = Image::make($request->portada->getRealPath());
            $image_resize->resize(800, 800, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
            });
            $image_resize->orientate();
            $minuscula = strtolower($request->titulo."-".$request->title);
            $nombre_minuscula = str_replace(" ", "-", $minuscula);
            $nombre_archivo = $nombre_minuscula . "." . $request->portada->extension();
            $image_resize->save(public_path('img/imgArticles/' . $nombre_archivo,50));
            $url = 'imgArticles/'.$nombre_archivo;
            $articulo->portada = $url;
        }

        //verifica que el objeto instanciado haya tenido alguna modificacion en sus atributos
        if(!$articulo->isDirty()){
             return back()->with('info','El articulo '.$articulo->titulo.' no fue actualizado, debe modificar algun atributo.');
        }

        $articulo->save();
        return redirect()->back()->with('info','El articulo '.$articulo->titulo.' fue actualizado.');

        
    }


    public function imprimirQR ($id){

        $articulo = Article::findOrFail($id);

        $pdf = PDF::loadView('admin.articulos.pdf_article',compact('articulo'));
        $minuscula = strtolower($articulo->titulo);
        $nombre_minuscula = str_replace(" ", "-", $minuscula);  
        return $pdf->download($nombre_minuscula.'.pdf');


    }


    public function comentarios ($id){

        $articulo = Article::with('comments')->findOrFail($id);
        return view('admin.articulos.comments',compact('articulo'));

    }


    public function eliminarComentario($id)
    {
        $comentario = Comment::findOrFail($id);
        //$this->authorize('passArticle', $articulo); // si es mio elimina el articulo (Control de Acceso)
        $comentario->delete();
        return back()->with('info','Se ha eliminado el comentario con exito !.');
    }




}
