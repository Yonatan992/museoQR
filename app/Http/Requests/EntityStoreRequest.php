<?php

namespace App\Http\Requests;

use App\Rules\AlphaSpace;
use Illuminate\Foundation\Http\FormRequest;

class EntityStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'locality_id'   => 'required|exists:localities,id', 
                'nombre'        => 'required|unique:entities,nombre|string|max:150',
                'direccion'     => 'required|unique:entities,direccion|string|max:250',
                'telefono'      => 'required|numeric|min:11',
                'email'         => 'required|email|unique:entities,email|max:50',
                'pagina'        => 'nullable|string|max:255',
                'facebook'      => 'nullable|string|max:255',
                'twitter'       => 'nullable|string|max:255',
                'instagram'     => 'nullable|string|max:255',
                'longitud'      => 'required|max:20',
                'latitud'       => 'required|max:20',
                'portada'       => 'required|mimes:jpeg,jpg,png|max:2048'
        ];
    }
}
