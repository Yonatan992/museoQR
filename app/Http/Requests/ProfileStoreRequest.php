<?php

namespace App\Http\Requests;

use App\Rules\AlphaSpace;
use Illuminate\Foundation\Http\FormRequest;

class ProfileStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        'nombre' => ['required',new AlphaSpace(),'unique:profiles,nombre','max:200' ],
        'name' => ['required', new AlphaSpace(),'unique:profiles,name','max:200' ],
        'descripcion' => ['nullable' ],
        ];
    }
}
