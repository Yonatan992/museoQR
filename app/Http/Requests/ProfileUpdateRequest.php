<?php

namespace App\Http\Requests;

use App\Rules\AlphaSpace;
use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'nombre'=> ['required',new AlphaSpace(),'max:200','unique:profiles,nombre,'.$this->profile->id],
        //$this-> le digo que evalue todo menos el id menos al el
           'name' => ['required',new AlphaSpace(),'max:200','unique:profiles,name,'.$this->profile->id],
           'descripcion'=> 'nullable|string',

       ];
   }
}
