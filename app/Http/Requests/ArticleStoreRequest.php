<?php

namespace App\Http\Requests;

use Illuminate\Http\store;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class ArticleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => 'required|exists:categories,id', 
            'entity_id'     => 'required|exists:entities,id', 
            'titulo'        => 'required|unique:articles,titulo|string|max:150',
            'title'         => 'required|unique:articles,title|string|max:150',
            'subtitulo'     => 'required|string|max:200',
            'subtitle'      => 'required|string|max:200',
            'descripcion'   => 'required|string',
            'description'   => 'required|string',
            'cuerpo'        => 'required|string',
            'body'          => 'required|string',
            'status'        => 'required|in:PUBLISHED,DRAFT',
            'portada'       => 'required|mimes:jpeg,jpg,png|max:2048',
            'urlVideo'      => 'nullable|url',

            //'portada' => $request->portada->store('')

        ];
    }
}
