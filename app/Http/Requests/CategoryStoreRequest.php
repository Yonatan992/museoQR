<?php

namespace App\Http\Requests;


use App\Rules\AlphaSpace;
use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
        'nombre'=> ['required',new AlphaSpace(),'unique:categories,nombre','max:200' ],
        'name'  => ['required', new AlphaSpace(),'unique:categories,name','max:200' ],
        'image' => 'nullable|mimes:jpeg,jpg,png|max:2048'
    ];
        /*return [
            'nombre'=> 'required','unique:categories,nombre','max:200', new AlphaSpace,
            'name' => 'required|max:200',
        ];*/
    }
}
