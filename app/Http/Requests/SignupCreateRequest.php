<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignupCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_id'    => 'required|exists:profiles,id',
            'entity_id'     => '', 
            'lang'          => 'nullable|in:es,en',
            'firstname'     => 'required|string',
            'lastname'      => 'required|string',
            'email'         => 'required|string|email|unique:users',
            'password'      => 'required|string|min:8'
        ];
    }
}
