<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => 'required|exists:categories,id', 
            'entity_id'     => 'required|exists:entities,id', 
            'titulo'        => 'required|string|max:150|unique:articles,titulo,'. $this->id,
            'title'         => 'required|string|max:150|unique:articles,title,'. $this->id,
            'subtitulo'     => 'required|string|max:200',
            'subtitle'      => 'required|string|max:1200',
            'descripcion'   => 'required|string',
            'description'   => 'required|string',
            'cuerpo'        => 'required|string',
            'body'          => 'required|string',
            'status'        => 'required|in:PUBLISHED,DRAFT',
            'portada'       => 'nullable|mimes:jpeg,jpg,png|max:2048',
            'urlVideo'      => 'nullable|url'
        ];
    }
}
