<?php

namespace App\Http\Requests;

use App\Rules\AlphaSpace;
use Illuminate\Foundation\Http\FormRequest;

class LocalityUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'province_id'=> 'required|exists:provinces,id',
                'nombre'=> ['required',new AlphaSpace(),'max:200','unique:localities,nombre,'.$this->locality->id]
        ];
    }
}
