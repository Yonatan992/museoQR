<?php

namespace App\Http\Middleware;

use Closure;

class signatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $header = 'X-Name')
    {
        $response = $next($request);
        // se asigna el nombre de la aplicacion a la cabecera
        $response->headers->set($header, config('app.name'));

        return $response;
    }
}
