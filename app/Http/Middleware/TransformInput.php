<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\ValidationException;

class TransformInput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $transformer)
    {
        $transformerInput = [];
        foreach ($request->request->all() as $input => $value) {
            $transformerInput [$transformer::originalAttribute($input)] = $value;
            
        }
        $request->replace($transformerInput);
        
         $response = $next($request);

         if (isset($response->exception) && $response->exception instanceof ValidationException){
            $data = $response->getData();

            $transformedErrors = [];

            foreach ($data->error as $field => $error) {
                $transformerField = $transformer::transformedAttribute($field);

                //funcion str_replace => recibe el campo original, el valor por el cual lo vamos a reemplazar y en donde lo vamos a reemplazar --
                $transformedErrors[$transformerField] = str_replace($field, $transformerField, $error);
                
            }

            $data->error = $transformedErrors;
            $response->setData($data);

         }

         return $response;
    }
}
