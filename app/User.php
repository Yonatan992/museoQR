<?php

namespace App;

use App\Models\Entity;
use App\Models\Profile;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\notify;
use App\Transformers\UserTransformer;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\PasswordResetNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    use SoftDeletes;

    const USUARIO_VERIFICADO    = '1';
    const USUARIO_NO_VERIFICADO = '0';
    const SUPER_ADMNISTRADOR    = '1';
    const ADMINISTRADOR         = '2';
    const VISITANTE             = '3';


    public $transformer = UserTransformer::class;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'profile_id',
       'entity_id',
       'lang',
       'firstname',
       'lastname',
       'email', 
       'password',
       'verified',
       'verification_token',
    ];
    
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'verification_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->belongsTo(Profile::class, 'profile_id','id');
    }

    public function entity(){
        return $this->belongsTo(Entity::class, 'entity_id','id');
    }


    public function esSuperAdmin ()
    {
        return $this->profile_id == User::SUPER_ADMNISTRADOR;
    }

    public function esAdmin ()
    {
        return $this->profile_id == User::ADMINISTRADOR;
    }

    public function esVisitante ()
    {
        return $this->profile_id == User::VISITANTE;
    }

    public function esVerificado ()
    {
        return $this->verified == User::USUARIO_VERIFICADO;
    }

    public static function generarVerificationToken()
    {
        return str_random(40);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }


    
}
