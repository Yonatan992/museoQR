<?php

namespace App\Exceptions;

use Exception;
use App\Traits\ApiResponser;
use Asm89\Stack\CorsService;
use Illuminate\Http\Concerns\acceptsHtml;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    use ApiResponser;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
   protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\validation\ValidationException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,

];
    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
       if ($exception instanceof ValidationException){
            return $this->convertValidationExceptionToResponse($exception,$request);
        }

        if ($exception instanceof ModelNotFoundException){
            $modelo = class_basename($exception->getModel());
            return $this->errorResponse("No Existe ninguna instancia de {$modelo} con el id expecificado", 404);
        }

        if ($exception instanceof NotFoundHttpException) {
            if ($request->is('api/*')) {
                return $this->errorResponse('Pagina no encontrada',404);
            }
            return response()->view('errors.404');
        }

        if ($exception instanceof MethodNotAllowedHttpException){
        
        return $this->errorResponse('El metodo especificado en la peticion no es valido', 405);

        }


        //controlamos de manera general cualquier exception de tipo HTTP
        if ($exception instanceof HttpException){
        
            return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
        }

        if ($exception instanceof QueryException){ 
            $codigo = $exception->errorInfo[1]; //la posicion 1 del parametro o atributo errorInfo para verificar el codigo de error
            if ($codigo == 1451){
                return $this->errorResponse('No se puede eliminar de forma permanente el recurso porque esta relacionado con algun otro.', 409);
            }
        }

        if ($exception instanceof TokenMismatchException ){
            return redirect()->back()->withInput($request->input());
        }


         // si estamos en modo depuracion el metodo render hara su trabajo habitual en caso contrario retornamos la respuesta de falla inesperada
        if (config('app.debug')){ //verificamos si la aplicacion esta en modo depuracion

             return parent::render($request, $exception);
        }
        //500 problema del servidor
        return $this->errorResponse('Falla inesperada. Intente luego', 500);






        if ($exception instanceof AuthenticationException){
            return $this->unauthenticated($request,$exception);
        }

        if ($exception instanceof AuthenticationException){
            return $this->errorResponse('No posee permisos para ejecutar esta accion', 403);
        }

    }



     protected function unauthenticated($request, AuthenticationException $exception)
    {
        
            if($this->isFrontend($request)){
                return redirect()->guest('login');
            }
            

            return $this->errorResponse('No Autentificado.', 401);
        
    }

    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {

            $errors = $e->validator->errors()->getMessages();
            
            if($this->isFrontend($request)){
                return $request->ajax() ? response()->json($errors, 422) : redirect()
                ->back()
                ->withInput($request->input())
                ->withErrors($errors);
            }

        return $this->errorResponse($errors,422);
    }

// Valida que los clientes realicen peticiones HTML y validar que al menos uno de los MIDDLEWARE utilizado en la ruta de esta peticion sea un middleware WEB (puede ser uno como pueden ser varios)
    private function isFrontend ($request){
        return $request->acceptsHtml() && collect($request->route()->middleware())->contains('web');
    }
}
