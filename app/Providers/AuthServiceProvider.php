<?php

namespace App\Providers;

use Carbon\Carbon;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();

        //agregamos segundos a los tokens (un tokens va hacer valido hasta 30 segundos)
        // addMinutes y addHours
        //Passport::tokensExpireIn(Carbon::now()->addSeconds(120));
        Passport::tokensExpireIn(Carbon::now()->addDays(15));

        //generar un nuevo tokens apartir del cual ya expiro (despues de 30 dias podra generar un nuevo tokens)
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));

        //generar un nuevo para Personal Access Tokens
        //Passport::personalAccessTokensExpireIn(now()->addSeconds(120));

        //
    }
}
