<?php

namespace App\Providers;

use App\User;
use App\Mail\UserCreated;
use App\Mail\UserMailChanged;
use Illuminate\Support\Carbon;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        Schema:: defaultStringLength(191);


        // el metodo retry recibe los intentos, la funcion a ejecutar y el tiempo que va a pasar en milisegundos entre un intento y otro
        User::created(function($user){
            retry(5, function() use($user) {
                Mail::to($user)->send(new UserCreated($user));
            }, 100);

        });

        User::updated(function($user){
            if ($user->isDirty('email')) {
                retry(5, function() use($user) {
                    Mail::to($user)->send(new UserMailChanged($user));
                }, 100);
            }
            

        });

        View::share('theme','lte');

    }
}
