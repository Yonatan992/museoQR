<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Passwords\getEmailForPasswordReset;
use \Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class PasswordResetNotification extends ResetPassword
{
    
    
    public function toMail($notifiable)
    {

        if ($notifiable->lang == 'es'){
             return (new MailMessage)
            ->subject('Recuperar contraseña')
            ->greeting('Hola')
            ->line(Lang::get('Se solicitó un restablecimiento de contraseña para tu cuenta ' . $notifiable->getEmailForPasswordReset() . ', haz clic en el botón que aparece a continuación para cambiar tu contraseña.'))
            ->action('Recuperar contraseña', url(route('password.reset', $this->token,false)))
            ->line('Si no realizaste esta solicitud, por favor ignore el mensaje.')
            ->salutation('Saludos, '. config('app.name'));


        } elseif ($notifiable->lang == 'en') {

               return (new MailMessage)
            ->subject('Reset password')
            ->greeting('Hello')
            ->line(Lang::get('Request a password reset for your account' . $notifiable->getEmailForPasswordReset() . ', click the button below to change your password.'))
            ->action('Recover password', url(route('password.reset', $this->token,false)))
            ->line('If you did not make this request, please ignore the message')
            ->salutation('Saludos, '. config('app.name'));

        }
       
    }

   
}
