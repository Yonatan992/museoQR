<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\ProvinceTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
	use SoftDeletes;
    public $transformer = ProvinceTransformer::class;
    protected $table = 'provinces';
    protected $primaryKey = 'id';
    protected $fillable = ['country_id','nombre'];
    public $timestamps = true;
	protected $dates = ['deleted_at'];

    public function country(){
    	return $this->belongsTo(Country::class,'country_id','id');
    }

    public function localities(){
    	return $this->hasMany(Locality::class,'province_id');
    }



}
