<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\ProfileTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;
    public $transformer = ProfileTransformer::class;
    protected $table = 'profiles';
    protected $primaryKey = 'id';

    protected $fillable = ['nombre','name','descripcion','description'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function users(){
    	return $this->hasMany(User::class,'user_id');
    }

}
