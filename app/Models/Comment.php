<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\CommentTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
	use SoftDeletes;
    public $transformer = CommentTransformer::class;
    protected $table = 'comments';
    protected $primaryKey = 'id';
    
    protected $fillable = ['article_id','user_id', 'valoracion','descripcion','respuesta'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function article(){
    	return $this->belongsTo(Article::class,'comment_id','id');
    }

    public function user(){
    	return $this->belongsTo(User::class,'user_id','id')->withTrashed();
    }
}
