<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\LocalityTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Locality extends Model
{
	use SoftDeletes;
    public $transformer = LocalityTransformer::class;
    protected $table = 'localities';
    protected $primaryKey = 'id';

    protected $fillable = ['province_id','nombre'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

   public function province(){
    	return $this->belongsTo(Province::class,'province_id','id');
    }

 public function entities(){
    	return $this->hasMany(Entity::class,'entity_id');
    }

}
