<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\CountryTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
	use SoftDeletes;
	public $transformer = CountryTransformer::class;
    protected $table = 'countries';
    protected $primaryKey = 'id';
    protected $fillable = [ 'nombre', 'name'];
    public $timestamps = true;
	protected $dates = ['deleted_at'];

     public function provinces(){
    	return $this->hasMany(Province::class, 'country_id');
    }
}
