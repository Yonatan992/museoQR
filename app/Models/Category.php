<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\CategoryTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

	use SoftDeletes;
	public $transformer = CategoryTransformer::class;
    protected $table = 'categories';
    protected $primaryKey = 'id';

    protected $fillable = ['nombre','name','image'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function articles(){
    	return $this->hasMany(Article::class, 'category_id');
    }

}
