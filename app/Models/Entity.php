<?php

namespace App\Models;

use App\User;
use App\Transformers\EntityTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entity extends Model
{

	use SoftDeletes;
    public $transformer = EntityTransformer::class;
    protected $table = 'entities';
    protected $primaryKey = 'id';

    protected $fillable = ['locality_id','nombre','direccion','telefono','email','pagina' ,'facebook','twitter','instagram','longitud','latitud','portada'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

     public function locality(){
    	return $this->belongsTo(Locality::class, 'locality_id','id');
    }

    public function articles(){
    	return $this->hasMany(Article::class, 'entity_id');
    }

    public function users(){
        return $this->hasMany(User::class,'entity_id');
    }
}
