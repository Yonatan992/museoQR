<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\ArticleTransformer;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{

    use SoftDeletes;
    use Sluggable;

    public $transformer = ArticleTransformer::class;
    protected $table = 'articles';
    protected $primaryKey = 'id';
    
    protected $fillable = ['category_id','entity_id', 'titulo','title','subtitulo','subtitle','descripcion','description','cuerpo','body','status','portada','urlVideo','codeQr','slug'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['titulo','title','entity.nombre']
            ]
        ];
    }

    public function category(){
    	return $this->belongsTo(Category::class, 'category_id','id');
    }

    public function entity(){
    	return $this->belongsTo(Entity::class,'entity_id','id');
    }

    public function comments(){
    	return $this->hasMany(Comment::class,'article_id');
    }

    public function code(){
        return $this->belongsTo(Code::class, 'code_id');
    }


}
