<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserMailChanged extends Mailable
{
    use Queueable, SerializesModels;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        // automaticamemnte laravel inyecta en la vista los atributos que tengamos en nuestro mailable
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if($this->user->lang == 'es') {
            return $this->markdown('emails.confirm')->subject('Por favor confirma tu correo electronico');
        } elseif ($this->user->lang == 'en') {
            return $this->markdown('emails.confirm')->subject('Please confirm your email');

        }
        
        
    }
}
