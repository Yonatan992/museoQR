<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        // automaticamemnte laravel inyecta en la vista los atributos que tengamos en nuestro mailable
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->user->lang == 'es') {
            return $this->markdown('emails.welcome')->subject('Gracias por Crear una cuenta');
        } elseif ($this->user->lang == 'en') {
            return $this->markdown('emails.welcome')->subject('Thanks for creating an account');

        }
    }
}
