<?php

namespace App\Transformers;

use App\Models\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Comment $comment)
    {
        return [
                'identificador'      => (int)$comment->id,
                'articulo'           => (int)$comment->article_id,
                'usuario'            => (int)$comment->user_id,
                'puntuacion'         => (String)$comment->valoracion,
                'detalle'            => (String)$comment->descripcion,
                'reaccion'           => (String)$comment->respuesta,
                'fechaCreacion'      => (String)$comment->created_at,
                'fechaActualizacion' => (String)$comment->updated_at,
                'fechaEliminacion'   => isset($comment->updated_at) ? (String) $comment->deleted_at : null,

                'link'  => [
                    [
                        'rel'  => 'self',
                        'href' => route('comments.show', $comment->id),

                    ],

                ],
            
            
        ];


    }

     public static function originalAttribute($index)
    {
        $attributes = [
                'identificador'      => 'id',
                'articulo'           => 'article_id',
                'usuario'            => 'user_id',
                'puntuacion'         => 'valoracion',
                'detalle'            => 'descripcion',
                'reaccion'           => 'respuesta',
                'fechaCreacion'      => 'created_at',
                'fechaActualizacion' => 'updated_at',
                'fechaEliminacion'   => 'deleted_at',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }


    public static function transformedAttribute($index)
    {
        $attributes = [
                'id'               =>   'identificador',
                'article_id'       =>   'articulo',
                'user_id'          =>   'usuario',
                'valoracion'       =>   'puntuacion',
                'descripcion'      =>   'detalle',
                'respuesta'        =>   'reaccion',
                'created_at'       =>   'fechaCreacion',
                'updated_at'       =>   'fechaActualizacion',
                'deleted_at'       =>   'fechaEliminacion',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
