<?php

namespace App\Transformers;

use App\Models\Article;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Article $article)
    {
         return [
                'identificador'      => (int)$article->id,
                'categoria'          => (int)$article->category_id,
                'categoriaNom'       => (String)$article->category->nombre,
                'entidad'            => (int)$article->entity_id,
                'entidadNom'         => (String)$article->entity->nombre,
                'tituloEs'           => (String)$article->titulo,
                'tituloEn'           => (String)$article->title,
                'subtituloEs'        => (String)$article->subtitulo,
                'subtituloEn'        => (String)$article->subtitle,
                'descripcionEs'      => (String)$article->descripcion,
                'descripcionEn'      => (String)$article->description,
                'cuerpoEs'           => (String)$article->cuerpo,
                'cuerpoEn'           => (String)$article->body,
                'slug'               => (String)$article->slug,
                'estado'             => (String)$article->status,
                'imagen'             => url("img/{$article->portada}"),
                'video'              => (String)$article->urlVideo,
                'qr'                 => url ("img/{$article->codeQr}"),
                'fechaCreacion'      => (String)$article->created_at,
                'fechaActualizacion' => (String)$article->updated_at,
                'fechaEliminacion'   => isset($article->updated_at) ? (String) $article->deleted_at : null,
                'denominacionCat' => $article->category->nombre,
                'denominacionCatEn' => $article->category->name,
                'denominacionEnt'  => $article->entity->nombre,
                'comments'         => $article->comments->map(function ($value,$key){
                  return [
                    'idComment'     => $value->id,
                    'valoracion'    => $value->valoracion,
                    'comentario'    => $value->descripcion,
                    'idUser'        => $value->user->id,
                    'apellido'      => $value->user->lastname,
                    'nombre'        => $value->user->firstname,
                    'respuesta'     => $value->respuesta,
                    'fecha'         => (String)$value->created_at,
                  ];
                }),

                
            
        ];

    }

    public static function originalAttribute($index)
    {
        $attributes = [
                'identificador'      => 'id',
                'categoria'          => 'category_id',
                'entidad'            => 'entity_id',
                'tituloEs'           => 'titulo',
                'tituloEn'           => 'title',
                'subtituloEs'        => 'subtitulo',
                'subtituloEn'        => 'subtitle',
                'descripcionEs'      => 'descripcion',
                'descripcionEn'      => 'description',
                'cuerpoEs'           => 'cuerpo',
                'cuerpoEn'           => 'body',
                'slug'               => 'slug',
                'estado'             => 'status',
                'imagen'             => 'portada',
                'video'              => 'urlVideo',
                'qr'                 => 'codeQr',
                'fechaCreacion'      => 'created_at',
                'fechaActualizacion' => 'updated_at',
                'fechaEliminacion'   => 'deleted_at',
                'fechaActualizacion' => 'updated_at',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

     public static function transformedAttribute($index)
    {
        $attributes = [
          'id'              =>'identificador',
          'category_id'     =>'categoria',   
          'entity_id'       =>'entidad',           
          'titulo'          =>'tituloEs',          
          'title'           =>'tituloEn',          
          'subtitulo'       =>'subtituloEs',      
          'subtitle'        =>'subtituloEn',       
          'descripcion'     =>'descripcionEs',     
          'description'     =>'descripcionEn',     
          'cuerpo'          =>'cuerpoEs',          
          'body'            =>'cuerpoEn',
          'slug'            =>'slug',          
          'status'          =>'estado',            
          'portada'         =>'imagen',            
          'urlVideo'        =>'video',             
          'codeQr'          =>'qr',                
          'created_at'      =>'fechaCreacion',     
          'updated_at'      =>'fechaActualizacion',
          'deleted_at'      =>'fechaEliminacion',  
          'updated_at'      =>'fechaActualizacion',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
