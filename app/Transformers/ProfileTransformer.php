<?php

namespace App\Transformers;

use App\Models\Profile;
use League\Fractal\TransformerAbstract;

class ProfileTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Profile $profile)
    {
        return [
                'identificador'      => (int)$profile->id,
                'alias'              => (String)$profile->nombre,
                'aliasEn'            => (String)$profile->name,
                'detalle'            => (String)$profile->descripcion,
                'detalleEn'          => (String)$profile->description,
                'fechaCreacion'      => (String)$profile->created_at,
                'fechaActualizacion' => (String)$profile->updated_at,
                'fechaEliminacion'   => isset($profile->updated_at) ? (String) $profile->deleted_at : null,

                'link'  => [
                    [
                        'rel'  => 'self',
                        'href' => route('profiles.show', $profile->id),

                    ],


                ],
            
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
                'identificador'      => 'id',
                'alias'              => 'nombre',
                'aliasEn'            => 'name',
                'detalle'            => 'descripcion',
                'detalleEn'         => 'description',
                'fechaCreacion'      => 'created_at',
                'fechaActualizacion' => 'updated_at',
                'fechaEliminacion'   => 'deleted_at',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
                 'id'               => 'identificador', 
                 'nombre'           => 'alias', 
                 'name'             => 'aliasEn', 
                 'descripcion'      => 'detalle', 
                 'description'      => 'detalleEn', 
                 'created_at'       => 'fechaCreacion', 
                 'updated_at'       => 'fechaActualizacion', 
                 'deleted_at'       => 'fechaEliminacion', 
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
