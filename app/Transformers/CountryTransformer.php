<?php

namespace App\Transformers;


use App\Models\Country;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Country $country)
    {
        return [
                'identificador'      => (int)$country->id,
                'nombreEs'           => (String)$country->nombre,
                'nombreEn'           => (String)$country->name,
                'fechaCreacion'      => (String)$country->created_at,
                'fechaActualizacion' => (String)$country->updated_at,
                'fechaEliminacion'   => isset($country->updated_at) ? (String) $country->deleted_at : null,

                'link'  => [
                    [
                        'rel'  => 'self',
                        'href' => route('countries.show', $country->id),

                    ],

                ],
            
        ];
    }

     public static function originalAttribute($index)
    {
        $attributes = [
                'identificador'      => 'id',
                'nombreEs'           => 'nombre',
                'nombreEn'           => 'name',
                'fechaCreacion'      => 'created_at',
                'fechaActualizacion' => 'updated_at',
                'fechaEliminacion'   => 'deleted_at',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

     public static function transformedAttribute($index)
    {
        $attributes = [
                 'id'               =>  'identificador', 
                 'nombre'           =>  'nombreEs', 
                 'name'             =>  'nombreEn', 
                 'created_at'       =>  'fechaCreacion', 
                 'updated_at'       =>  'fechaActualizacion', 
                 'deleted_at'       =>  'fechaEliminacion', 
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
