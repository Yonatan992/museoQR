<?php

namespace App\Transformers;

use App\Models\Entity;
use League\Fractal\TransformerAbstract;

class EntityTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Entity $entity)
    {
        return [
                'identificador'      => (int)$entity->id,
                'localidad'          => (int)$entity->locality_id,
                'provincia'          => $entity->locality->province->id,
                'pais'               => $entity->locality->province->country->id,
                'denominacion'       => (String)$entity->nombre,
                'ubicacion'          => (String)$entity->direccion,
                'telefono'           => (int)$entity->telefono,
                'correo'             => (String)$entity->email,
                'direccionWeb'       => (String)$entity->pagina,
                'facebook'           => (String)$entity->facebook,
                'twitter'            => (String)$entity->twitter,
                'instagram'          => (String)$entity->instagram,
                'longitud'           => (double)$entity->longitud,
                'latitud'            => (double)$entity->latitud,
                'imagen'             => url("img/{$entity->portada}"),
                'fechaCreacion'      => (String)$entity->created_at,
                'fechaActualizacion' => (String)$entity->updated_at,
                'fechaEliminacion'   => isset($entity->updated_at) ? (String) $entity->deleted_at : null,       
                'denominacionL' => $entity->locality->nombre,
                'denominacionPro' => $entity->locality->province->nombre,
                'denominacionPro' => $entity->locality->province->nombre,
                'denominacionPa' => $entity->locality->province->country->nombre,


                'link'  => [
                    [
                        'rel'  => 'self',
                        'href' => route('entities.show', $entity->id),

                    ],

                    [
                        'rel'  => 'entities.articles',
                        'href' => route ('entities.articles.index', $entity->id),

                    ],

                ],


            
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
                'identificador'      => 'id',
                'localidad'          => 'locality_id',
                'denominacion'       => 'nombre',
                'ubicacion'          => 'direccion',
                'telefono'           => 'telefono',
                'correo'             => 'email',
                'direccionWeb'       => 'pagina',
                'facebook'           => 'facebook',
                'twitter'            => 'twitter',
                'instagram'          => 'instagram',
                'longitud'           => 'longitud',
                'latitud'            => 'latitud',
                'imagen'             => 'portada',
                'fechaCreacion'      => 'created_at',
                'fechaActualizacion' => 'updated_at',
                'fechaEliminacion'   => 'deleted_at',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
                 'id'               => 'identificador', 
                 'locality_id'      => 'localidad', 
                 'nombre'           => 'denominacion', 
                 'direccion'        => 'ubicacion', 
                 'telefono'         => 'telefono', 
                 'email'            => 'correo', 
                 'pagina'           => 'direccionWeb',
                 'facebook'         => 'facebook', 
                 'twitter'          => 'twitter', 
                 'instagram'        => 'instagram',  
                 'longitud'         => 'longitud', 
                 'latitud'          => 'latitud', 
                 'portada'          => 'imagen', 
                 'created_at'       => 'fechaCreacion', 
                 'updated_at'       => 'fechaActualizacion', 
                 'deleted_at'       => 'fechaEliminacion', 
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
