<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
         return [
                'identificador'      => (int)$user->id,
                'perfil'             => (int)$user->profile_id,
                'nombrePer'          => (string)$user->profile->nombre,
                'entidad'            => (String)$user->entity_id,
                'nombreEnt'          => isset($user->entity->nombre) ? (String)$user->entity->nombre : null,
                'lang'               => (String)$user->lang,
                'verified'           => (int)$user->verified,
                'nombre'             => (String)$user->firstname,
                'apellido'           => (String)$user->lastname,
                'correo'             => (String)$user->email,
                'contraseña'         => (String)$user->password,
                'verified'           => (int)$user->verified,
                'fechaCreacion'      => (String)$user->created_at,
                'fechaActualizacion' => (String)$user->updated_at,
                'fechaEliminacion'   => isset($user->updated_at) ? (String) $user->deleted_at : null,

                'link'  => [
                    [
                        'rel'  => 'self',
                        'href' => route('users.show', $user->id),

                    ],
                ],
            
        ];
    }

     public static function originalAttribute($index)
    {
        $attributes = [
                'identificador'      => 'id',
                'perfil'             => 'profile_id',
                'entidad'            => 'entity_id',
                'lang'               => 'lang',
                'nombre'             => 'firstname',
                'apellido'           => 'lastname',
                'correo'             => 'email',
                'verified'           => 'verified',
                'fechaCreacion'      => 'created_at',
                'fechaActualizacion' => 'updated_at',
                'fechaEliminacion'   => 'deleted_at',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

     public static function transformedAttribute($index)
    {
        $attributes = [
                 'id'               => 'identificador',      
                 'profile_id'       => 'perfil',
                 'entity_id'        => 'entidad', 
                 'lang'             => 'lang',                        
                 'firstname'        => 'nombre',
                 'lastname'         => 'apellido',            
                 'email'            => 'correo',            
                 'verified'         => 'verified',            
                 'created_at'       => 'fechaCreacion',      
                 'updated_at'       => 'fechaActualizacion',
                 'deleted_at'       => 'fechaEliminacion',   
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
