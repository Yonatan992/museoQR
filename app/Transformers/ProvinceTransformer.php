<?php

namespace App\Transformers;

use App\Models\Province;
use League\Fractal\TransformerAbstract;

class ProvinceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Province $province)
    {
         return [
                'identificador'      => (int)$province->id,
                'pais'               => (String)$province->country_id,
                'nombrePais'         => $province->country->nombre,
                'denominacion'       => (String)$province->nombre,
                'fechaCreacion'      => (String)$province->created_at,
                'fechaActualizacion' => (String)$province->updated_at,
                'fechaEliminacion'   => isset($province->updated_at) ? (String) $province->deleted_at : null,
                

                'link'  => [
                    [
                        'rel'  => 'self',
                        'href' => route('provinces.show', $province->id),

                    ],

                ],
            
        ];
    }

     public static function originalAttribute($index)
    {
        $attributes = [
                'identificador'      => 'id',
                'pais'               => 'country_id',
                'denominacion'       => 'nombre',
                'fechaCreacion'      => 'created_at',
                'fechaActualizacion' => 'updated_at',
                'fechaEliminacion'   => 'deleted_at',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
                 'id'                => 'identificador',      
                 'country_id'        => 'pais',               
                 'nombre'            => 'denominacion',       
                 'created_at'        => 'fechaCreacion',     
                 'updated_at'        => 'fechaActualizacion',
                 'deleted_at'        => 'fechaEliminacion',   
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
