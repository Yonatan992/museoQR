<?php

namespace App\Transformers;

use App\Models\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
                'identificador'      => (int)$category->id,
                'aliasEs'            => (String)$category->nombre,
                'aliasEn'            => (String)$category->name,
                'imagen'             => (String)$category->image,
                'fechaCreacion'      => (String)$category->created_at,
                'fechaActualizacion' => (String)$category->updated_at,
                'fechaEliminacion'   => isset($category->updated_at) ? (String) $category->deleted_at : null,

                'link'  => [
                    [
                        'rel'  => 'self',
                        'href' => route('categories.show', $category->id),

                    ],

                    [
                        'rel'  => 'categories.articles',
                        'href' => route ('categories.articles.index', $category->id),

                    ],
                ],
            
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
                'identificador'         => 'id',
                'aliasEs'               => 'nombre',
                'aliasEn'               => 'name',
                'imagen'                => 'image',
                'fechaCreacion'         => 'created_at',
                'fechaActualizacion'    => 'updated_at',
                'fechaEliminacion'      => 'deleted_at',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
                'id'            =>  'identificador',    
                'nombre'        =>  'aliasEs',           
                'name'          =>  'aliasEn',
                'image'         =>  'imagen',           
                'created_at'    =>  'fechaCreacion',     
                'updated_at'    =>  'fechaActualizacion',
                'deleted_at'    =>  'fechaEliminacion',  
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
