<?php

namespace App\Transformers;

use App\Models\Locality;
use League\Fractal\TransformerAbstract;

class LocalityTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Locality $locality)
    {
        return [
                'identificador'      => (int)$locality->id,
                'denominacion'       => (String)$locality->nombre,
                'provincia'          => (String)$locality->province_id,
                'nombrePro'          => (String)$locality->province->nombre,
                'pais'               => (String)$locality->province->country_id,
                'nombrePais'         => (String)$locality->province->country->nombre,
                'fechaCreacion'      => (String)$locality->created_at,
                'fechaActualizacion' => (String)$locality->updated_at,
                'fechaEliminacion'   => isset($locality->updated_at) ? (String) $locality->deleted_at : null,
                'link'  => [
                    [
                        'rel'  => 'self',
                        'href' => route('localities.show', $locality->id),

                    ],

                ],
            
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
                'identificador'      => 'id',
                'provincia'          => 'province_id',
                'denominacion'       => 'nombre',
                'fechaCreacion'      => 'created_at',
                'fechaActualizacion' => 'updated_at',
                'fechaEliminacion'   => 'deleted_at',
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
                'id'                => 'identificador', 
                'province_id'       => 'provincia', 
                'nombre'            => 'denominacion', 
                'created_at'        => 'fechaCreacion', 
                'updated_at'        => 'fechaActualizacion', 
                'deleted_at'        => 'fechaEliminacion', 
            
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
