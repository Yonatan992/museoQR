<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('article_id')->unsigned();           
            $table->foreign('article_id')->references('id')->on('articles');

            $table->integer('user_id')->unsigned();           
            $table->foreign('user_id')->references('id')->on('users');

            $table->enum('valoracion',['1','2','3','4','5'])->default('1');
            $table->text('descripcion')->nullable();
            $table->text('respuesta')->nullable();


            $table->timestamps();
             $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
