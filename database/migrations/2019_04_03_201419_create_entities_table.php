<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locality_id')->unsigned();           
            $table->foreign('locality_id')->references('id')->on('localities');
            
            $table->string('nombre',150)->unique()->required();
            $table->string('direccion',250);
            $table->string('telefono');
            $table->string('email',100)->unique();
            $table->string('pagina')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->decimal('longitud', 10,7);
            $table->decimal('latitud', 10,7);
            $table->string('portada', 200)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entities');
    }
}
