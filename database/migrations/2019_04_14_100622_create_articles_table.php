<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id')->unsigned();           
            $table->foreign('category_id')->references('id')->on('categories');

            $table->integer('entity_id')->unsigned();           
            //$table->foreign('entity_id')->references('id')->on('entities');
            
            $table->string('titulo',150)->unique()->required();
            $table->string('title',150)->unique()->required();
            $table->string('subtitulo',200)->required();
            $table->string('subtitle',200)->required();
            $table->text('descripcion')->required();
            $table->text('description')->required();
            $table->text('cuerpo')->required();;
            $table->text('body')->required();;
            $table->enum('status', ['PUBLISHED','DRAFT'])->default('DRAFT'); //PUBLICADO / BORRADOR
            $table->string('portada', 200)->nullable();
            $table->string('urlVideo')->nullable();
            $table->string('codeQr')->nullable();
            $table->string('slug')->unique();

            $table->timestamps();
            $table->softDeletes();
            $table->foreign('entity_id')->references('id')->on('entities')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
