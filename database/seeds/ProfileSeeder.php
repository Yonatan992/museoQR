<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
    	DB::table('profiles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas

        DB::table('profiles')->insert([
            'nombre' => 'Super Administrador',
            'name' => 'Super Administrator',
            'descripcion' => 'Perfil que tiene control total sobre la APP',
            'description' => 'Profile that has full control over the APP',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('profiles')->insert([
            'nombre' => 'Administrador',
            'name' => 'Administrator',
            'descripcion' => 'Perfil que tiene control sobre los recursos del Museo',
            'description' => ' Profile that has control over the resources of the Museum',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

         DB::table('profiles')->insert([
            'nombre' => 'Visitante',
            'name' => 'Visitor',
            'descripcion' => 'Perfil con acceso a las vistas publicas de la APP',
            'description' => 'Profile with access to public views of the APP',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

    }
}
