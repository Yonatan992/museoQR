<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
        DB::table('comments')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas




         DB::table('comments')->insert([
            'article_id' => 1,
            'user_id' => 4,
            'valoracion' => 3,
            'descripcion' => 'Muy buena informacion',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

          DB::table('comments')->insert([
            'article_id' => 1,
            'user_id' => 5,
            'valoracion' => 2,
            'descripcion' => 'Muy buena todo',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


          DB::table('comments')->insert([
            'article_id' => 3,
            'user_id' => 6,
            'valoracion' => 5,
            'descripcion' => 'Muy buena informacion',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

          DB::table('comments')->insert([
            'article_id' => 1,
            'user_id' => 7,
            'valoracion' => 4,
            'descripcion' => 'Muy buena todo',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


          DB::table('comments')->insert([
            'article_id' => 2,
            'user_id' => 6,
            'valoracion' => 5,
            'descripcion' => 'Muy buena informacion',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

          DB::table('comments')->insert([
            'article_id' => 2,
            'user_id' => 7,
            'valoracion' => 4,
            'descripcion' => 'Muy buena todo',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


          DB::table('comments')->insert([
            'article_id' => 5,
            'user_id' => 6,
            'valoracion' => 5,
            'descripcion' => 'Muy buena informacion',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

          DB::table('comments')->insert([
            'article_id' => 5,
            'user_id' => 7,
            'valoracion' => 4,
            'descripcion' => 'Muy buena todo',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


          DB::table('comments')->insert([
            'article_id' => 6,
            'user_id' => 8,
            'valoracion' => 5,
            'descripcion' => 'Muy buena informacion',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

          DB::table('comments')->insert([
            'article_id' => 6,
            'user_id' => 9,
            'valoracion' => 4,
            'descripcion' => 'Muy buena todo',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);



          DB::table('comments')->insert([
            'article_id' => 4,
            'user_id' => 11,
            'valoracion' => 5,
            'descripcion' => 'Muy buena informacion',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

          DB::table('comments')->insert([
            'article_id' => 4,
            'user_id' => 12,
            'valoracion' => 4,
            'descripcion' => 'Muy buena todo',
            'respuesta' => 'Muchas Gracias',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


          DB::table('comments')->insert([
            'article_id' => 7,
            'user_id' => 11,
            'valoracion' => 5,
            'descripcion' => 'Muy buena informacion',
            'respuesta' => '',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

          DB::table('comments')->insert([
            'article_id' => 1,
            'user_id' => 12,
            'valoracion' => 4,
            'descripcion' => 'Muy buena todo',
            'respuesta' => '',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('comments')->insert([
          'article_id' => 11,
          'user_id' => 12,
          'valoracion' => 3,
          'descripcion' => 'Muy buena todo',
          'respuesta' => '',
          'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s'),
      ]);

      DB::table('comments')->insert([
        'article_id' => 12,
        'user_id' => 12,
        'valoracion' => 4,
        'descripcion' => 'Muy buena todo',
        'respuesta' => '',
        'created_at' => date('Y-m-d H:i:s'),
    'updated_at' => date('Y-m-d H:i:s'),
    ]);

    DB::table('comments')->insert([
          'article_id' => 13,
          'user_id' => 12,
          'valoracion' => 4,
          'descripcion' => 'Muy buena todo',
          'respuesta' => '',
          'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s'),
      ]);

      DB::table('comments')->insert([
        'article_id' => 11,
        'user_id' => 11,
        'valoracion' => 5,
        'descripcion' => 'Muy buena informacion',
        'respuesta' => '',
        'created_at' => date('Y-m-d H:i:s'),
    'updated_at' => date('Y-m-d H:i:s'),
    ]);

    DB::table('comments')->insert([
      'article_id' => 11,
      'user_id' => 9,
      'valoracion' => 4,
      'descripcion' => 'Muy buena todo',
      'respuesta' => 'Muchas Gracias',
      'created_at' => date('Y-m-d H:i:s'),
  'updated_at' => date('Y-m-d H:i:s'),
  ]);

  DB::table('comments')->insert([
    'article_id' => 11,
    'user_id' => 6,
    'valoracion' => 5,
    'descripcion' => 'Muy buena informacion',
    'respuesta' => 'Muchas Gracias',
    'created_at' => date('Y-m-d H:i:s'),
'updated_at' => date('Y-m-d H:i:s'),
]);

DB::table('comments')->insert([
  'article_id' => 11,
  'user_id' => 4,
  'valoracion' => 4,
  'descripcion' => 'Muy buena informacion',
  'respuesta' => 'Muchas Gracias',
  'created_at' => date('Y-m-d H:i:s'),
'updated_at' => date('Y-m-d H:i:s'),
]);
    }
}
