<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
    	DB::table('categories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas

        DB::table('categories')->insert([
            'nombre' => 'Mamiferos',
            'name' => 'Mammals',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'nombre' => 'Aves',
            'name' => 'Birds',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


        DB::table('categories')->insert([
            'nombre' => 'Peces',
            'name' => 'Fishes',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


        DB::table('categories')->insert([
            'nombre' => 'Roedores',
            'name' => 'Rodents',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'nombre' => 'Cuadros',
            'name' => 'Picture',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'nombre' => 'Jarrones',
            'name' => 'Vases',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'nombre' => 'Barcos',
            'name' => 'Boats',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

    }
}
