<?php

use Illuminate\Database\Seeder;

class LocalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
    	DB::table('localities')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas

        DB::table('localities')->insert([
            'province_id' => 1,
            'nombre' => 'Corrientes',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('localities')->insert([
            'province_id' => 1,
            'nombre' => 'Mercedes',
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
