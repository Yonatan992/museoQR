<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revision de claves foraneas
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revision de claves foraneas

        

        // -- Administradord de la aplicacion --
        
        DB::table('users')->insert([
            'profile_id' => 1,
            'entity_id' => null,
            'firstname' => 'Yonantan',
            'lastname' => 'Sanchez',
            'email' => 'yonatan@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

 		// ---- Administrador de entidad -------

        DB::table('users')->insert([
            'profile_id' => 2,          
            'entity_id' => 1,
            'firstname' => 'Ezequiel',
            'lastname' => 'Gimenez',
            'email' => 'eze@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


        DB::table('users')->insert([
            'profile_id' => 2,          
            'entity_id' => 2,
            'firstname' => 'Carlos',
            'lastname' => 'Romero',
            'email' => 'carlos@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


        // ----- Visitantes ------


         DB::table('users')->insert([
            'profile_id' => 3,
            'entity_id' => null,
            'firstname' => 'Gaston',
            'lastname' => 'Sena',
            'email' => 'gaston@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

           DB::table('users')->insert([
            'profile_id' => 3,
            'entity_id' => null,
            'firstname' => 'Jorge',
            'lastname' => 'Rodriguez',
            'email' => 'pepe@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

          DB::table('users')->insert([
            'profile_id' => 3,
            'entity_id' => null,
            'firstname' => 'Ramon',
            'lastname' => 'Ramirez',
            'email' => 'ramon@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

         DB::table('users')->insert([
            'profile_id' => 3,
            'entity_id' => null,
            'firstname' => 'Sergio',
            'lastname' => 'Seiber',
            'email' => 'sergio@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

         DB::table('users')->insert([
            'profile_id' => 3,
            'entity_id' => null,
            'firstname' => 'JuanManuel',
            'lastname' => 'Ojeda',
            'email' => 'juan@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);


         DB::table('users')->insert([
            'profile_id' => 3,
            'entity_id' => null,
            'firstname' => 'Silvana',
            'lastname' => 'Peloso',
            'email' => 'silvana@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

         DB::table('users')->insert([
            'profile_id' => 3,
            'entity_id' => null,
            'firstname' => 'Noelia',
            'lastname' => 'Geromini',
            'email' => 'noelia@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);
         DB::table('users')->insert([
            'profile_id' => 3,
            'entity_id' => null,
            'firstname' => 'Noelia',
            'lastname' => 'Sanchez',
            'email' => 'noe@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'profile_id' => 3,
            'entity_id' => null,
            'firstname' => 'Dahiana',
            'lastname' => 'Villagra',
            'email' => 'dai@gmail.com',
            'password' => bcrypt('123456'),
            'verified' => User::USUARIO_NO_VERIFICADO,
            'verification_token'=>User::generarVerificationToken(),
            'created_at' => date('Y-m-d H:i:s'),
    		'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
